# Contributing to atlas-fe CLI

Thank you for considering a contribution to atlas-fe CLI! Pull requests, issues and comments are welcome. For pull requests, please:

- Add tests for new features and bug fixes
- Follow the existing style
- Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, please make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate link below to digitally sign the CLA. The Corporate CLA is for those who are contributing as a member of an organization and the individual CLA is for those contributing as an individual.

- [CLA for corporate contributors](https://opensource.atlassian.com/corporate)
- [CLA for individuals](https://opensource.atlassian.com/individual)

## Installation

1. Install the dependencies of this project:

```sh
yarn install
```

2. Link all the packages locally:

```sh
yarn packages:link
```

## Development

### Configuring Maven

If you are including Atlassian SDK into a pipeline, use Maven version provided in the Docker image instead of older Maven version from Atlassian SDK bundle by adding `export ATLAS_MVN=/usr/bin/mvn` after installing Atlassian SDK like this:

```yaml
script:
  # Install the Atlassian SDK. Our generated scripts depend on theirs.
  - curl -sS https://packages.atlassian.com/api/gpg/key/public | apt-key add -
  - echo "deb https://packages.atlassian.com/debian/atlassian-sdk-deb/ stable contrib" >>/etc/apt/sources.list
  - apt-get update && apt-get install --no-install-recommends -y atlassian-plugin-sdk
  # Use Maven provided in the Docker image instead of older version of Maven bundled in Atlassian SDK
  - export ATLAS_MVN=/usr/bin/mvn
```

Then you will need to add `atlassian-public` Maven repository into the `pom.xml` files for both environments so that the system Maven can find Atlassian packages.
```xml

<project>
    ...
    <repositories>
        <repository>
            <id>atlassian-public</id>
            <url>https://packages.atlassian.com/mvn/maven-external/</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>atlassian-public</id>
            <url>https://packages.atlassian.com/mvn/maven-external/</url>
        </pluginRepository>
    </pluginRepositories>
    ...
</project>
```

### Using refapp for development

Use the refapp environment to test the CLI locally:

```sh
cd ./environments/refapp

atlas-fe <command>
```

### Using your own plugin for development

If you prefer, you can also use the CLI locally on your own AMPS plugin:

```sh
atlas-create-{jira|confluence|bitbucket}-plugin your-plugin

cd your-plugin

atlas-fe <command>
```

### Unlinking packages

If you want to use the globally installed version, remember to unlink the local packages.
Execute this command in the root of the repository:

```sh
yarn packages:unlink
```

### Clean output

Clean all the `build` folders in each package by running this command in the root of the repository:

```sh
yarn clean
```

### Watch mode

Start watch mode in all packages by running this command in the root of the repository:

```sh
yarn watch
```

## Unit tests

Unit tests are written using Jest, creating a file with the same name of the file to test, in the same directory, and adding the `.test` suffix before the extension.

Example:

- init.ts [file to test]
- init.test.ts [tests]

You can execute the tests for all packages using:

```sh
yarn test
```

And execute all the tests in watch mode using:

```sg
yarn test --watch
```

## E2E test

E2E tests use Jest and Puppeteer. They create a temporary testing environment for each case and then perform the tests.

Puppeteer accepts the following options using env variables:

- HEADLESS (true|false): run the test in headless mode. Is `false` by default.
- SLOWMO (number): delay in ms to run actions in puppeteer. Is `0` by default.

Example:

```sh
HEADLESS=false SLOWMO=300 yarn e2e:default-settings
```

## Releasing

To release any new version, you need to have access to use Pipelines.

### Stable release

To perform a stable release:

1.  Create a new branch locally.
2.  Execute `yarn packages:version` and select the new stable version you want to release.
    This will create a commit with all the packages bumped to the new version.
3.  Verify if the CHANGELOG.md is up to date, and add anything missing if needed.
4.  Push the changes and create a PR.
5.  Once the PR is approved and builds are green, go to the Pipeline of your branch and click "Deploy".
6.  The release process will publish all the packages to NPM and create a new tag in the repository.

### Tagged release

You can use tagged releases to test a version before an official release (e.g: snapshot or beta).

To do so:

1. Go to Pipelines.
2. Click "Run Pipeline"
3. Select a branch you would like to do a tagged release.
4. Select "custom: tagged release" pipeline.
5. Specify a VERSION (e.g: 0.2.0-rc-1) and a DIST_TAG (e.g: next), and click "Run".
