# atlas-fe CLI

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

The atlas-fe CLI is the missing half of [Atlassian SDK (AMPS)](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/) for frontend developers.

By executing a single command on top of your P2 plugin, the CLI will install and configure a fully working
frontend development environment for you. This includes Webpack, WRM webpack plugin, TypeScipt, ESlint/Prettier and React!

[TOC]

## Requirements

- Node v20+
- [Atlassian SDK (AMPS)](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/)

## Installation

Install the package globally using npm:

```sh
npm install --global @atlassian/atlas-fe
```

or yarn:

```sh
yarn global add @atlassian/atlas-fe
```

## Breaking changes

### 0.4.0

Since version 0.4.0 onwards, there are several changes that needed to be aware of:
- Dependencies now requires Maven 3.6.0+, use newer version of Maven on your system instead of older version of Maven bundled in Atlassian SDK by setting `ATLAS_MVN` path to your newer Maven version on your system:
  ```shell
  # Docker image or Linux
  export ATLAS_MVN=/usr/bin/mvn

  # MacOS
  export ATLAS_MVN=$HOME/bin/mvn

  # If you use mvnvm
  export ATLAS_MVN=$HOME/.jenv/shims/mvn
  ```
- By using Node.js newer than 15.x, Node.js would throw an error when there is any unhandled rejection in the code. Until this has been resolved, you would need to set `NODE_OPTIONS=--unhandled-rejections=warn` to make it flagged as warning instead of error when do E2E testing:
  ```shell
  export NODE_OPTIONS=--unhandled-rejections=warn
  ```
- atlassian-webresource-webpack-plugin 5 now disables `addEntrypointNameAsContext` and `addAsyncNameAsContext` by default. If you migrate
  from atlas-fe 0.3.x to 0.4.x and your app doesn't load, please enable those 2 options into the WRMPlugin configuration of chainWebpackConfig in the `atlas-fe.config.js` file. In general, `addEntrypointNameAsContext` should be enough.
  ```javascript
  chainWebpackConfig: (webpackConfig) => {
    webpackConfig.plugin('WrmPlugin').tap(([config]) => {
      return [
        {
          ...config,
          addEntrypointNameAsContext: true,
          addAsyncNameAsContext: true,
          // other custom config ...
        },
      ];
    });
  }
  ```
  For the newly created apps using 0.4.x, the app URL is on the path `<BASE_URL>/plugins/servlet/atlas-fe-tests` e.g. http://localhost:5990/refapp/plugins/servlet/atlas-fe-tests

## Usage

### 1. Create a plugin for your target product using AMPS:

```sh
## For Jira
atlas-create-jira-plugin your-plugin

## For Confluence
atlas-create-confluence-plugin your-plugin

## For Bitbucket
atlas-create-bitbucket-plugin your-plugin
```

### 2. Go into the plugin's directory and execute `atlas-fe init` command.

This will install and configure frontend tools (like Webpack, WRM webpack plugin, etc) and
integrate the setup with AMPS by generating a valid maven configuration in your `pom.xml`.

```sh
cd ./your-plugin

atlas-fe init
```

### 3. Start your plugin by executing:

```sh
npm start

# or yarn

yarn start
```

You're now ready to develop your Atlassian P2 plugin with modern frontend tooling 🥳

## Demo

Watch all you can do with atlas-fe CLI in this demo: https://www.youtube.com/watch?v=pVYyS_83uX4

## Documentation

### Get started

Learn how to initialize your project with atlas-fe CLI, what each of the files generated means
and how to develop Atlassian P2 plugins in the [getting started](./docs/guides/getting-started.md) guides.

### Configure your project as you need

Use your own Babel/ESLint/Prettier setup, add more web-resources to your P2 plugin, configure provided dependencies
or completly change the internal webpack configuration by reading the
[configuring atlas-fe CLI](./docs/guides/configuration.md) guides

### Share your configuration with plugins

If you work on a cross product team, or you maintain multiple Atlassian plugins,
you can learn how to share the same configuration across all your projects by
[creating a plugin](./docs/guides/creating-plugins.md) for atlas-fe CLI.

## Roadmap

Here's a list of plugins and configurations planned or under development:

- ✅ Babel
- ✅ ESLint
- ✅ Prettier
- ✅ TypeScript (tsconfig, babel, eslint, prettier config)
- ✅ React (babel, eslint, prettier config)
- ✅ Client-side Extensions
- ✅ i18n (i18n-loader, wrm-react-i18n)
- [ ] Styles (css, less, sass, css modules)
- [ ] Static assets (images, files)
- [ ] Jest (JS and TS setup)
- [ ] Cypress

## Contributions

Contributions to atlas-fe CLI are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

[![With ❤,  from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-cheers-light.png)](https://www.atlassian.com)
