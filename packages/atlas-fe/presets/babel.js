module.exports = () => {
  return {
    presets: [require('@babel/preset-env')],
    plugins: [
      require('@babel/plugin-syntax-dynamic-import'),
      require('@babel/plugin-proposal-class-properties'),
      require('@babel/plugin-transform-runtime'),
    ],
  };
};
