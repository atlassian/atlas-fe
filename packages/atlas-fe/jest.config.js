/* eslint-disable @typescript-eslint/no-var-requires */
const base = require('../../jest.base.config.js');
const pack = require('./package.json');

module.exports = {
  ...base,
  displayName: pack.name,
  name: pack.name,
};
