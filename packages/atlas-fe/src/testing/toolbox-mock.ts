import { GluegunToolbox, build, filesystem, http, packageManager, patching, print, semver, strings, system } from 'gluegun';
import { Writable } from 'stream';

import { AtlasFeToolbox } from '../types';
import { AtlasFeConfig } from '../toolbox/config';
import extendWithAtlasFe from '../extensions/api';

// mocks copied from + some of our own:
// https://github.com/infinitered/solidarity/blob/master/__tests__/__mocks__/mockContext.ts

export type TestingReadAsync = (path: string) => Promise<string | undefined>;
export type TestingWriteAsync = (path: string, content: Writable, { jsonIndent }: { jsonIndent: number }) => void;

export interface ToolboxMock extends GluegunToolbox {
  config: {
    loadConfig: (name: string, src: string) => unknown;
    isDevelopment?: boolean;
  };
}

const gluegunToolboxMock: ToolboxMock = {
  build,
  filesystem,
  http,
  patching,
  semver,
  strings,
  system,

  config: {
    loadConfig: jest.fn(() => ({
      entries: {},
      pluginKey: 'a.fake.plugin-key',
    })),
  },

  print: {
    ...print,
    error: jest.fn(),
    success: jest.fn(),
    info: jest.fn(),
    warning: jest.fn(),
    newline: jest.fn(),
    divider: jest.fn(),
    checkmark: '✓',
    spin: jest.fn(() => ({
      start: jest.fn(),
      stop: jest.fn(),
      fail: jest.fn(),
      succeed: jest.fn(),
      stopAndPersist: jest.fn(),
      clear: jest.fn(),
      warn: jest.fn(),
      info: jest.fn(),
      render: jest.fn(),
      frame: jest.fn(),
      color: 'white',
      isSpinning: false,
      text: 'fake',
      prefixText: 'fake',
      spinner: 'dots',
      indent: 2,
    })),
    table: jest.fn(),
  },

  prompt: {
    ask: jest.fn(),
    confirm: jest.fn(),
    separator: jest.fn(),
  },

  template: {
    generate: jest.fn(),
  },

  parameters: {
    options: {},
  },

  generate: jest.fn(),

  meta: {
    src: 'fake',
    version: jest.fn(),
    packageJSON: jest.fn(),
    commandInfo: jest.fn(),
    checkForUpdate: jest.fn(),
    onAbort: jest.fn(),
  },

  packageManager: {
    ...packageManager,
    hasYarn: jest.fn(() => false),
  },
};

const extendedToolbox = extendWithAtlasFe(gluegunToolboxMock);

const atlasFeToolboxMock: AtlasFeToolbox = {
  ...extendedToolbox,

  /**
   * Only mock what uses filesystem APIs to avoid having to create a tmp folder.
   */

  config: {
    loadConfig: gluegunToolboxMock.config.loadConfig as () => AtlasFeConfig,
    isDevelopment: true,
    entries: {
      'mock-js': './mock.js',
    },
    pluginKey: 'mock-plugin-key',
    output: '/mock/output',
  },

  pom: {
    ...extendedToolbox.pom,
    getPomContent: jest.fn(),
    writePomContent: jest.fn(),
    getPluginKeyFromPom: jest.fn(),
  },

  packageJson: {
    read: jest.fn(),
    write: jest.fn(),
  },

  plugins: {
    exists: jest.fn(),
    run: jest.fn(),
    load: jest.fn(),
  },
};

export const chainWrmPluginMock = (toolbox: AtlasFeToolbox) => {
  class WrmPluginMock {
    constructor(private options: Record<string, unknown>) {}

    apply(compiler: unknown) {
      return compiler;
    }
  }

  toolbox.webpack.chainWebpackConfig((config) => {
    config.plugin('WrmPlugin').use(WrmPluginMock, [
      {
        pluginKey: 'fake-plugin',
      },
    ]);
  });
};

export { gluegunToolboxMock, atlasFeToolboxMock };
