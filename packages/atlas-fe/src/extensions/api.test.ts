import extend from './api';
import { gluegunToolboxMock } from '../testing/toolbox-mock';
import { AtlasFeToolbox } from '../types';

describe('toolbox API', () => {
  it('should contain all the atlas-fe APIs', () => {
    const testingToolbox = { ...gluegunToolboxMock };
    const extendedToolbox = extend(testingToolbox);

    expect(extendedToolbox).toEqual<AtlasFeToolbox>({
      // if config contains the `isDevelopment` property, is safe to asume the object is correct
      config: expect.objectContaining({ isDevelopment: true }),
      webpack: {
        chainWebpackConfig: expect.any(Function),
        addWrmProvidedDependencies: expect.any(Function),
        resolveWebpackConfig: expect.any(Function),
      },
      pom: {
        parseXml: expect.any(Function),
        serializeXml: expect.any(Function),
        getPomContent: expect.any(Function),
        writePomContent: expect.any(Function),
        getPluginKeyFromPom: expect.any(Function),
      },
      packageJson: {
        read: expect.any(Function),
        write: expect.any(Function),
      },
      template: {
        render: expect.any(Function),
        generate: expect.any(Function),
      },
      plugins: {
        exists: expect.any(Function),
        run: expect.any(Function),
        load: expect.any(Function),
      },
      detectIndent: expect.any(Function),

      // Gluegun API. We don't really care about their signatures in this test.
      build: expect.anything(),
      filesystem: expect.anything(),
      http: expect.anything(),
      packageManager: expect.anything(),
      patching: expect.anything(),
      prompt: expect.anything(),
      semver: expect.anything(),
      strings: expect.anything(),
      system: expect.anything(),
      print: expect.anything(),
      parameters: expect.anything(),
      generate: expect.anything(),
      meta: expect.anything(),
    });
  });
});
