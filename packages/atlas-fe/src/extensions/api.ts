import { GluegunToolbox } from 'gluegun';

import { AtlasFeConfigToolbox, getConfig } from '../toolbox/config';
import { chainWebpackConfig, resolveWebpackConfig, addWrmProvidedDependencies, AtlasFeWebpackToolbox } from '../toolbox/webpack';
import { getPomContent, writePomContent, getPluginKeyFromPom, AtlasFePomToolbox, parseXml, serializeXml } from '../toolbox/pom';
import { AtlasFePackageJson, readPackageJson, writePackageJson, WritePackageJsonCallback } from '../toolbox/package-json';
import { AtlasFeTemplateToolbox, render } from '../toolbox/template';
import { AtlasFePluginsToolbox, exists, run, loadPlugins } from '../toolbox/plugins';
import { AtlasFeDetectIndentToolbox, detectIndent } from '../toolbox/detect-indent';
import { PomAsJson } from '../types';

export type AtlasFeToolbox = GluegunToolbox &
  AtlasFeConfigToolbox &
  AtlasFeWebpackToolbox &
  AtlasFePomToolbox &
  AtlasFePackageJson &
  AtlasFeDetectIndentToolbox &
  AtlasFeTemplateToolbox &
  AtlasFePluginsToolbox;

const extend = (toolbox: GluegunToolbox) => {
  // First, load the user configuration and set development mode on/off
  const config = getConfig(toolbox);

  // Then, add each of the core extensions (APIs)
  (toolbox as AtlasFeToolbox).config = config;

  (toolbox as AtlasFeToolbox).webpack = {
    chainWebpackConfig,
    resolveWebpackConfig,
    addWrmProvidedDependencies,
  };

  (toolbox as AtlasFeToolbox).pom = {
    parseXml,
    serializeXml,
    getPomContent: () => getPomContent(toolbox),
    writePomContent: (content: PomAsJson, indent: string) => writePomContent(toolbox, content, indent),
    getPluginKeyFromPom: () => getPluginKeyFromPom(toolbox),
  };

  (toolbox as AtlasFeToolbox).packageJson = {
    read: () => readPackageJson(toolbox),
    write: (cb: WritePackageJsonCallback) => writePackageJson(cb, toolbox),
  };

  (toolbox as AtlasFeToolbox).template = {
    ...toolbox.template,
    render,
  };

  (toolbox as AtlasFeToolbox).plugins = {
    exists: (name: string) => exists(toolbox, name),
    run: (commandToExecute: string) => run(toolbox as AtlasFeToolbox, commandToExecute),
    load: (packages: string[]) => loadPlugins(toolbox as AtlasFeToolbox, packages),
  };

  (toolbox as AtlasFeToolbox).detectIndent = detectIndent;

  return toolbox as AtlasFeToolbox;
};

export default extend;
