import { GluegunToolbox } from 'gluegun';

import { getConfig } from '../toolbox/config';
import { loadPluginsFromConfig } from '../toolbox/plugins';

const extend = (toolbox: GluegunToolbox) => {
  // Get the plugin configuration. There is no guarantee that the `api` toolbox is going to be constructed before this
  // extension, so is better to get the config directly from the file again.
  const config = getConfig(toolbox);

  // this happens syncronously, no need to "wait"
  loadPluginsFromConfig(toolbox, config);
};

export default extend;
