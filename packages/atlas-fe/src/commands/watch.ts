import { AtlasFeCommand } from '../types';

const COMMAND_NAME = 'watch';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    // better start up performance if we load the utils when the command runs
    const { chainBaseWebpackConfig, chainUserWebpackConfig, startDevServer } = await import('../utils/webpack');
    const { chainBabelLoader } = await import('../utils/babel');
    const { chainI18n } = await import('../utils/i18n');

    toolbox.print.info('Watching your project...');

    chainBaseWebpackConfig(toolbox, { shouldWatch: true, shouldPrepare: false });

    chainBabelLoader(toolbox);

    chainI18n(toolbox, true);

    await toolbox.plugins.run(COMMAND_NAME);

    chainUserWebpackConfig(toolbox, COMMAND_NAME);

    startDevServer(toolbox);
  },
};

export default command;
