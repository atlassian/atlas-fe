import { mocked } from 'jest-mock';
import runAll from 'npm-run-all';

import command from './start';
import { AtlasFeToolbox } from '../types';
import { atlasFeToolboxMock } from '../testing/toolbox-mock';

jest.mock('npm-run-all', () => ({
  __esModule: true, // this property makes it work
  default: jest.fn(),
}));

describe('start command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };
  });

  it(`should be named "start"`, () => {
    expect(command.name).toBe('start');
  });

  it(`should print a message to the console when executing`, async () => {
    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.info)).toHaveBeenCalledWith('Starting your project...');
  });

  it(`should execute start:prepare and start:watch scripts in parallel`, async () => {
    const expectedOptions = {
      parallel: true,
      stderr: process.stderr,
      stdin: process.stdin,
      stdout: process.stdout,
    };

    await command.run(testingToolbox);

    expect(mocked(runAll)).toBeCalledWith(['start:prepare', 'start:watch'], expectedOptions);
  });
});
