import { AtlasFeCommand } from '../types';

const COMMAND_NAME = 'watch-prepare';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    // better start up performance if we load the utils when the command runs
    const { chainBaseWebpackConfig, chainUserWebpackConfig, runWebpack } = await import('../utils/webpack');
    const { chainBabelLoader } = await import('../utils/babel');
    const { chainI18n } = await import('../utils/i18n');

    toolbox.print.info('Preparing to watch your project...');

    chainBaseWebpackConfig(toolbox, { shouldWatch: true, shouldPrepare: true });

    chainBabelLoader(toolbox);

    chainI18n(toolbox, true);

    await toolbox.plugins.run(COMMAND_NAME);

    chainUserWebpackConfig(toolbox, COMMAND_NAME);

    runWebpack(toolbox);
  },
};

export default command;
