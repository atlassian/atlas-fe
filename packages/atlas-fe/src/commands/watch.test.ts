import { mocked } from 'jest-mock';

import command from './watch';
import { AtlasFeToolbox } from '../types';
import { atlasFeToolboxMock } from '../testing/toolbox-mock';
import { chainBaseWebpackConfig, chainUserWebpackConfig, startDevServer } from '../utils/webpack';
import { chainBabelLoader } from '../utils/babel';
import { chainI18n } from '../utils/i18n';

jest.mock('../utils/webpack', () => ({
  chainBaseWebpackConfig: jest.fn(),
  chainUserWebpackConfig: jest.fn(),
  startDevServer: jest.fn(),
}));

jest.mock('../utils/babel', () => ({
  chainBabelLoader: jest.fn(),
}));

jest.mock('../utils/i18n', () => ({
  chainI18n: jest.fn(),
}));

describe('watch command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };
  });

  it(`should be named "watch"`, () => {
    expect(command.name).toBe('watch');
  });

  it(`should print a message to the console when executing`, async () => {
    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.info)).toHaveBeenCalledWith('Watching your project...');
  });

  it(`should configure webpack and apply user config before starting webpack dev server`, async () => {
    const testingExecutionOrder: string[] = [];
    const expectedExecutionOrder = [
      'chainBaseWebpackConfig',
      'chainBabelLoader',
      'chainI18n',
      'toolbox.plugins.run',
      'chainUserWebpackConfig',
      'startDevServer',
    ];

    mocked(testingToolbox.plugins.run).mockImplementation(async () => {
      testingExecutionOrder.push('toolbox.plugins.run');
    });
    mocked(chainBaseWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainBaseWebpackConfig'));
    mocked(chainBabelLoader).mockImplementation(() => testingExecutionOrder.push('chainBabelLoader'));
    mocked(chainI18n).mockImplementation(() => testingExecutionOrder.push('chainI18n'));
    mocked(chainUserWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainUserWebpackConfig'));
    mocked(startDevServer).mockImplementation(() => testingExecutionOrder.push('startDevServer'));

    await command.run(testingToolbox);

    expect(testingExecutionOrder).toEqual(expectedExecutionOrder);
    expect(testingToolbox.plugins.run).toHaveBeenCalledWith(command.name);
  });

  it(`should configure webpack on watch mode`, async () => {
    await command.run(testingToolbox);

    expect(mocked(chainBaseWebpackConfig)).toHaveBeenCalledWith(testingToolbox, { shouldWatch: true, shouldPrepare: false });
  });
});
