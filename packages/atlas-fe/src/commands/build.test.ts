import { mocked } from 'jest-mock';

import command from './build';
import { AtlasFeToolbox } from '../types';
import { atlasFeToolboxMock } from '../testing/toolbox-mock';
import { chainBaseWebpackConfig, chainUserWebpackConfig, runWebpack } from '../utils/webpack';
import { chainBabelLoader } from '../utils/babel';
import { chainI18n } from '../utils/i18n';

jest.mock('../utils/webpack', () => ({
  chainBaseWebpackConfig: jest.fn(),
  chainUserWebpackConfig: jest.fn(),
  runWebpack: jest.fn(),
}));

jest.mock('../utils/babel', () => ({
  chainBabelLoader: jest.fn(),
}));

jest.mock('../utils/i18n', () => ({
  chainI18n: jest.fn(),
}));

describe('build command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };
  });

  it(`should be named "build"`, () => {
    expect(command.name).toBe('build');
  });

  it(`should print a message to the console when executing`, async () => {
    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.info)).toHaveBeenCalledWith('Building your project...');
  });

  it(`should configure webpack and apply user config before running webpack`, async () => {
    const testingExecutionOrder: string[] = [];
    const expectedExecutionOrder = [
      'chainBaseWebpackConfig',
      'chainBabelLoader',
      'chainI18n',
      'toolbox.plugins.run',
      'chainUserWebpackConfig',
      'runWebpack',
    ];

    mocked(testingToolbox.plugins.run).mockImplementation(async () => {
      testingExecutionOrder.push('toolbox.plugins.run');
    });
    mocked(chainBaseWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainBaseWebpackConfig'));
    mocked(chainBabelLoader).mockImplementation(() => testingExecutionOrder.push('chainBabelLoader'));
    mocked(chainI18n).mockImplementation(() => testingExecutionOrder.push('chainI18n'));
    mocked(chainUserWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainUserWebpackConfig'));
    mocked(runWebpack).mockImplementation(() => testingExecutionOrder.push('runWebpack'));

    await command.run(testingToolbox);

    expect(testingExecutionOrder).toEqual(expectedExecutionOrder);
    expect(testingToolbox.plugins.run).toHaveBeenCalledWith(command.name);
  });
});
