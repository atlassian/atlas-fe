import { AtlasFeCommand } from '../types';

const COMMAND_NAME = 'build';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    // better start up performance if we load the utils when the command runs
    const { chainBaseWebpackConfig, chainUserWebpackConfig, runWebpack } = await import('../utils/webpack');
    const { chainBabelLoader } = await import('../utils/babel');
    const { chainI18n } = await import('../utils/i18n');

    toolbox.print.info('Building your project...');

    chainBaseWebpackConfig(toolbox);

    chainBabelLoader(toolbox);

    chainI18n(toolbox);

    await toolbox.plugins.run(COMMAND_NAME);

    chainUserWebpackConfig(toolbox, COMMAND_NAME);

    runWebpack(toolbox);
  },
};

export default command;
