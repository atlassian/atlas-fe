import { AtlasFeCommand } from '../types';

const COMMAND_NAME = 'init';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    // better start up performance if we load the utils when the command runs
    const { askInitQuestions } = await import('../utils/init-questions');
    const { initPackageJson } = await import('../utils/package-json');
    const { installPlugins } = await import('../utils/plugins');
    const { generateStarterFiles } = await import('../utils/starter-files');
    const { generateMavenConfiguration } = await import('../utils/maven');
    const { configureBabelPreset } = await import('../utils/babel');
    const { configureESLintPreset } = await import('../utils/eslint');
    const { configurePrettierConfig } = await import('../utils/prettier');

    const initQueue = [
      askInitQuestions,
      initPackageJson,
      installPlugins,
      generateStarterFiles,
      generateMavenConfiguration,
      configureBabelPreset,
      configureESLintPreset,
      configurePrettierConfig,
    ];

    for (const initTask of initQueue) {
      await initTask(toolbox);
    }

    await toolbox.plugins.run(COMMAND_NAME);

    toolbox.print.newline();
    toolbox.print.success(`Setup ready! Run '${toolbox.packageManager.hasYarn() ? 'yarn' : 'npm'} start' to start your project.`);
    toolbox.print.newline();
  },
};

export default command;
