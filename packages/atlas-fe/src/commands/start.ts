import runAll from 'npm-run-all';

import { AtlasFeCommand } from '../types';

const COMMAND_NAME = 'start';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: (toolbox) => {
    toolbox.print.info('Starting your project...');

    runAll(['start:prepare', 'start:watch'], { parallel: true, stderr: process.stderr, stdin: process.stdin, stdout: process.stdout });
  },
};

export default command;
