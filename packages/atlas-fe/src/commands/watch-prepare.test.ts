import { mocked } from 'jest-mock';

import command from './watch-prepare';
import { AtlasFeToolbox } from '../types';
import { atlasFeToolboxMock } from '../testing/toolbox-mock';
import { chainBaseWebpackConfig, chainUserWebpackConfig, runWebpack } from '../utils/webpack';
import { chainBabelLoader } from '../utils/babel';
import { chainI18n } from '../utils/i18n';

jest.mock('../utils/webpack', () => ({
  chainBaseWebpackConfig: jest.fn(),
  chainUserWebpackConfig: jest.fn(),
  runWebpack: jest.fn(),
}));

jest.mock('../utils/babel', () => ({
  chainBabelLoader: jest.fn(),
}));

jest.mock('../utils/i18n', () => ({
  chainI18n: jest.fn(),
}));

describe('watch-prepare command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };
  });

  it(`should be named "watch-prepare"`, () => {
    expect(command.name).toBe('watch-prepare');
  });

  it(`should print a message to the console when executing`, async () => {
    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.info)).toHaveBeenCalledWith('Preparing to watch your project...');
  });

  it(`should configure webpack and apply user config before starting webpack dev server`, async () => {
    const testingExecutionOrder: string[] = [];
    const expectedExecutionOrder = [
      'chainBaseWebpackConfig',
      'chainBabelLoader',
      'chainI18n',
      'toolbox.plugins.run',
      'chainUserWebpackConfig',
      'runWebpack',
    ];

    mocked(testingToolbox.plugins.run).mockImplementation(async () => {
      testingExecutionOrder.push('toolbox.plugins.run');
    });
    mocked(chainBaseWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainBaseWebpackConfig'));
    mocked(chainBabelLoader).mockImplementation(() => testingExecutionOrder.push('chainBabelLoader'));
    mocked(chainI18n).mockImplementation(() => testingExecutionOrder.push('chainI18n'));
    mocked(chainUserWebpackConfig).mockImplementation(() => testingExecutionOrder.push('chainUserWebpackConfig'));
    mocked(runWebpack).mockImplementation(() => testingExecutionOrder.push('runWebpack'));

    await command.run(testingToolbox);

    expect(testingExecutionOrder).toEqual(expectedExecutionOrder);
    expect(testingToolbox.plugins.run).toHaveBeenCalledWith(command.name);
  });

  it(`should configure webpack on watch mode and wrm on prepare mode`, async () => {
    await command.run(testingToolbox);

    expect(mocked(chainBaseWebpackConfig)).toHaveBeenCalledWith(testingToolbox, { shouldWatch: true, shouldPrepare: true });
  });
});
