import { mocked } from 'jest-mock';

import command from './init';
import { AtlasFeToolbox } from '../types';
import { atlasFeToolboxMock } from '../testing/toolbox-mock';
import { askInitQuestions } from '../utils/init-questions';
import { initPackageJson } from '../utils/package-json';
import { installPlugins } from '../utils/plugins';
import { generateStarterFiles } from '../utils/starter-files';
import { generateMavenConfiguration } from '../utils/maven';
import { configureBabelPreset } from '../utils/babel';
import { configureESLintPreset } from '../utils/eslint';
import { configurePrettierConfig } from '../utils/prettier';

jest.mock('../utils/init-questions', () => ({
  askInitQuestions: jest.fn(),
}));
jest.mock('../utils/package-json', () => ({
  initPackageJson: jest.fn(),
}));
jest.mock('../utils/plugins', () => ({
  installPlugins: jest.fn(),
}));
jest.mock('../utils/starter-files', () => ({
  generateStarterFiles: jest.fn(),
}));
jest.mock('../utils/maven', () => ({
  generateMavenConfiguration: jest.fn(),
}));
jest.mock('../utils/babel', () => ({
  configureBabelPreset: jest.fn(),
}));
jest.mock('../utils/eslint', () => ({
  configureESLintPreset: jest.fn(),
}));
jest.mock('../utils/prettier', () => ({
  configurePrettierConfig: jest.fn(),
}));

describe('init command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };
  });

  it(`should be named "init"`, () => {
    expect(command.name).toBe('init');
  });

  it(`should initialize the different parts of the project in the right order`, async () => {
    const testingExecutionOrder: string[] = [];
    const expectedExecutionOrder = [
      'askInitQuestions',
      'initPackageJson',
      'installPlugins',
      'generateStarterFiles',
      'generateMavenConfiguration',
      'configureBabelPreset',
      'configureESLintPreset',
      'configurePrettierConfig',
      'toolbox.plugins.run',
    ];

    mocked(testingToolbox.plugins.run).mockImplementation(async () => {
      testingExecutionOrder.push('toolbox.plugins.run');
    });
    mocked(askInitQuestions).mockImplementation(async () => {
      testingExecutionOrder.push('askInitQuestions');
    });
    mocked(initPackageJson).mockImplementation(async () => {
      testingExecutionOrder.push('initPackageJson');
    });
    mocked(installPlugins).mockImplementation(async () => {
      testingExecutionOrder.push('installPlugins');
    });
    mocked(generateStarterFiles).mockImplementation(async () => {
      testingExecutionOrder.push('generateStarterFiles');
    });
    mocked(generateMavenConfiguration).mockImplementation(async () => {
      testingExecutionOrder.push('generateMavenConfiguration');
    });
    mocked(configureBabelPreset).mockImplementation(async () => {
      testingExecutionOrder.push('configureBabelPreset');
    });
    mocked(configureESLintPreset).mockImplementation(async () => {
      testingExecutionOrder.push('configureESLintPreset');
    });
    mocked(configurePrettierConfig).mockImplementation(async () => {
      testingExecutionOrder.push('configurePrettierConfig');
    });

    await command.run(testingToolbox);

    expect(testingExecutionOrder).toEqual(expectedExecutionOrder);
    expect(testingToolbox.plugins.run).toHaveBeenCalledWith(command.name);
  });

  it(`should print a message to the console when execution finishes`, async () => {
    // Message with Yarn
    mocked(testingToolbox.packageManager.hasYarn).mockImplementation(() => true);

    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.success)).toHaveBeenCalledWith(`Setup ready! Run 'yarn start' to start your project.`);

    // Message with npm
    mocked(testingToolbox.packageManager.hasYarn).mockImplementation(() => false);

    await command.run(testingToolbox);

    expect(mocked(testingToolbox.print.success)).toHaveBeenCalledWith(`Setup ready! Run 'npm start' to start your project.`);
  });
});
