// Amplitude Analytics ID for CLI project
export const AMP_CLIENT_ID = 'b8a92216deaab059c108b4e2ce41eb12';

// Versions used by maven-frontend-plugin.
// Keep in sync with .nvmrc and package.json engine.
export const VERSIONS = {
  node: 'v20.15.1',
  yarn: 'v1.22.22',
};
