import path from 'path';
import fs from 'fs';
import { semver } from 'gluegun';

import { VERSIONS } from './constants';
import { PackageJSON } from './types';

// the .nvmrc in the root of the monorepo
const repositorytNvmrc = fs.readFileSync(path.resolve(__dirname, '..', '..', '..', '.nvmrc')).toString();
// the .nvmrc that we generate for plugins
const templateNvmrc = fs.readFileSync(path.resolve(__dirname, 'templates', '.nvmrc')).toString();
// the package.json file of atlas-fe package
const packageJson: PackageJSON = JSON.parse(fs.readFileSync(path.resolve(__dirname, '..', 'package.json')).toString());

describe('node version in pom template and .nvmrc files', () => {
  let expectedVersion: string;

  beforeAll(() => {
    expectedVersion = packageJson.engines?.node || '';
  });

  it('should be consistent with version in package.json engine', async () => {
    expect(semver.satisfies(repositorytNvmrc, expectedVersion));
    expect(semver.satisfies(templateNvmrc, expectedVersion));
    expect(semver.satisfies(VERSIONS.node, expectedVersion));
  });
});

describe('yarn verion in pom template', () => {
  let expectedVersion: string;

  beforeAll(() => {
    expectedVersion = packageJson.engines?.yarn || '';
  });

  it('should be consistent with the version in package.json engine', () => {
    expect(semver.satisfies(VERSIONS.yarn, expectedVersion));
  });
});
