import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import WrmPlugin from 'atlassian-webresource-webpack-plugin';
import formatWebpackMessages from 'webpack-format-messages';
import TerserPlugin from 'terser-webpack-plugin';

import { AtlasFeToolbox } from '../types';

const DEV_SERVER_PORT = 3333;
const DEV_SERVER_HOST = '127.0.0.1';
const DEV_SERVER_CONFIG: WebpackDevServer.Configuration = {
  host: DEV_SERVER_HOST,
  port: DEV_SERVER_PORT,
  allowedHosts: 'all',
  client: {
    overlay: {
      warnings: true,
      errors: true,
    },
  },
  hot: true,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
  },
};

export const chainBaseWebpackConfig = (toolbox: AtlasFeToolbox, { shouldWatch = false, shouldPrepare = false } = {}) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // Entries
    Object.entries(toolbox.config.entries).forEach(([key, value]) => {
      webpackConfig.entry(key).add(value);
    });
    // Output
    webpackConfig.output.path(toolbox.config.output).filename('[name].bundle.js');
    // Resolve
    webpackConfig.resolve.extensions.add('.js').add('*');
    // Use terser as minimizer
    webpackConfig.optimization.minimizer('terser').use(TerserPlugin).end();

    process.env.NODE_ENV === 'production' ? chainProductionConfig(toolbox) : chainDevelopmentConfig(toolbox);

    // Watch config
    shouldWatch && chainWatchConfig(toolbox);

    // WRM config
    chainWebpackWrmPlugin(toolbox, { shouldWatch, shouldPrepare });
  });
};

export const chainUserWebpackConfig = (toolbox: AtlasFeToolbox, command: string) => {
  toolbox.print.divider();
  toolbox.print.info(`Applying webpack configuration from atlas-fe.config.js`);
  toolbox.print.divider();

  toolbox.webpack.chainWebpackConfig(
    (webpackConfig) => toolbox.config.chainWebpackConfig && toolbox.config.chainWebpackConfig(webpackConfig, command),
  );

  toolbox.webpack.addWrmProvidedDependencies(toolbox.config.providedDependencies || {});

  toolbox.print.newline();
};

export const runWebpack = (toolbox: AtlasFeToolbox) => {
  const compiler = webpack(toolbox.webpack.resolveWebpackConfig());

  hookWebpackStats(toolbox, compiler);

  compiler.run((error, stats) => {
    if (error) {
      toolbox.print.error(`Error dev webpack: ${error.message}`);
      process.exit(1);
    }

    // No need to print since the webpack stats will do it for us
    if (stats?.hasErrors()) {
      process.exit(1);
    }
  });
};

export const startDevServer = (toolbox: AtlasFeToolbox) => {
  const compiler = webpack(toolbox.webpack.resolveWebpackConfig());

  const server = new WebpackDevServer(compiler, DEV_SERVER_CONFIG);

  hookWebpackStats(toolbox, compiler);

  // Listen to interrupt  signal to kill dev server
  process.on('SIGINT', () => {
    server.close(() => {
      process.exit(0);
    });
  });

  // Listen to terminate signal to kill dev server
  process.on('SIGTERM', () => {
    server.close(() => {
      process.exit(0);
    });
  });

  server.listen(DEV_SERVER_PORT, DEV_SERVER_HOST, (error) => {
    if (error) {
      toolbox.print.error(error.message);
      process.exit(1);
    }
  });
};

const chainDevelopmentConfig = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // Mode
    webpackConfig.mode('development');
    // Devtool
    webpackConfig.devtool('cheap-source-map');
    // Optimizations
    webpackConfig.optimization.set('minimize', false).set('runtimeChunk', false).set('splitChunks', false);
  });
};

const chainProductionConfig = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // Mode
    webpackConfig.mode('production');
    // Devtool
    webpackConfig.devtool(false);
    // Optimizations
    webpackConfig.optimization.set('minimize', true).set('runtimeChunk', false).set('splitChunks', {
      minSize: 0,
      chunks: 'all',
      maxInitialRequests: Infinity,
    });
  });
};

const chainWebpackWrmPlugin = (toolbox: AtlasFeToolbox, { shouldWatch = false, shouldPrepare = false } = {}) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // WRM plugin
    // atlassian-webresource-webpack-plugin 5 now disables addEntrypointNameAsContext and addAsyncNameAsContext by default.
    // If you're migrating from 0.3.x to 0.4.x and your app doesn't load, set those 2 options to true.
    webpackConfig.plugin('WrmPlugin').use(WrmPlugin, [
      {
        pluginKey: toolbox.config.pluginKey,
        xmlDescriptors: toolbox.filesystem.path(toolbox.config.output, 'META-INF', 'plugin-descriptors', 'wr-webpack-bundles.xml'),
        watch: shouldWatch,
        watchPrepare: shouldPrepare,
        providedDependencies: toolbox.config.providedDependencies || {},
      },
    ]);
  });
};

const chainWatchConfig = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    webpackConfig.output.publicPath(`http://${DEV_SERVER_HOST}:${DEV_SERVER_PORT}/`).filename('[name].js').chunkFilename('[name].chunk.js');
  });
};

const hookWebpackStats = (toolbox: AtlasFeToolbox, compiler: webpack.Compiler) => {
  compiler.hooks.done.tap('done', (stats) => {
    const messages = formatWebpackMessages(stats);

    if (messages.errors.length) {
      toolbox.print.error('Failed to compile.');
      messages.errors.forEach((e) => toolbox.print.info(e));
      return;
    }

    if (messages.warnings.length) {
      toolbox.print.error('Compiled with warnings.');
      messages.warnings.forEach((w) => toolbox.print.info(w));
      return;
    }

    toolbox.print.success('Compiled successfully!');
  });
};
