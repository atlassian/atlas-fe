import get from 'lodash/get';
import set from 'lodash/set';
import findIndex from 'lodash/findIndex';

import { AtlasFeToolbox, PomAsJson, PomPluginAsJson } from '../types';
import { VERSIONS } from '../constants';

const BUILD_COMMAND = {
  npm: 'run build',
  yarn: 'build',
};

const WATCH_PREPARE_COMMAND = {
  npm: 'run start:watch-prepare',
  yarn: 'start:watch-prepare',
};

const POM_PROPERTIES_PATH = 'project.0.properties.0';
const POM_PROFILES_PATH = 'project.0.profiles.0.profile';
const POM_PLUGINS_PATH = 'project.0.build.0.plugins.0.plugin';
const POM_INSTRUCTIONS_PATH = 'configuration.0.instructions.0'; // from POM_PLUGINS_PATH.[index], this is the path to get to instructions

export const generateMavenConfiguration = async (toolbox: AtlasFeToolbox) => {
  try {
    const { content: pom, indent } = await toolbox.pom.getPomContent();
    const { content: templatePom } = await getTemplatePom(toolbox);

    // side effect
    await writeAtlassianScanFolders(pom);

    // side effect
    await writeProperties(pom, templatePom);

    // side effect
    await writeFrontendPlugin(pom, templatePom);

    // side effect
    await writeBuildProfiles(pom, templatePom);

    await toolbox.pom.writePomContent(pom, indent);

    toolbox.print.info(`${toolbox.print.checkmark} Completed maven-frontend-plugin configuration`);
  } catch (error) {
    // print only the message of the error
    toolbox.print.error((error as Error).message);
    throw new Error((error as Error).message);
  }
};

const getTemplatePom = async (toolbox: AtlasFeToolbox) => {
  const templatePath = `${toolbox.plugin?.directory}/templates/pom.xml.ejs`;

  if (!toolbox.filesystem.exists(templatePath)) {
    throw new Error('atlas-fe: pom template not found.');
  }

  const templatePomRaw = await toolbox.filesystem.readAsync(templatePath);

  if (!templatePomRaw) {
    throw new Error('atlas-fe: pom template is empty.');
  }

  const packageManager = toolbox.packageManager.hasYarn() ? 'yarn' : 'npm';

  const templatePom = toolbox.template.render(templatePomRaw, {
    nodeVersion: VERSIONS.node,
    packageManager: packageManager,
    // if using npm, then the npm version used will be the one that comes with node. There's no need to specify the version.
    // https://github.com/eirslett/frontend-maven-plugin#installing-node-and-npm
    packageManagerVersion: packageManager === 'yarn' ? VERSIONS.yarn : null,
    buildCommand: BUILD_COMMAND[packageManager],
    watchPrepareCommand: WATCH_PREPARE_COMMAND[packageManager],
  });

  return toolbox.pom.parseXml<PomAsJson>(templatePom);
};

const writeAtlassianScanFolders = async (pom: PomAsJson) => {
  const pluginsDeclaration = get(pom, POM_PLUGINS_PATH, []) as PomPluginAsJson[];

  const atlassianDeclarationIndex = findIndex(pluginsDeclaration, (plugin) => {
    const instructions = get(plugin, POM_INSTRUCTIONS_PATH, {});
    return !!(instructions as { [key: string]: string })['Atlassian-Plugin-Key'];
  });

  if (atlassianDeclarationIndex < 0) {
    throw new Error('atlas-fe: No maven plugin found with an Atlassian-Plugin-Key instruction.');
  }

  // side effect
  set(
    pom,
    `${POM_PLUGINS_PATH}.${atlassianDeclarationIndex}.${POM_INSTRUCTIONS_PATH}['Atlassian-Scan-Folders']`,
    'META-INF/plugin-descriptors',
  );

  return pom;
};

const writeProperties = async (pom: PomAsJson, templatePom: PomAsJson) => {
  const pomAttributes = get(pom, POM_PROPERTIES_PATH, {});
  const templatePomAttributes = get(templatePom, POM_PROPERTIES_PATH, {});

  // side effect
  set(pom, POM_PROPERTIES_PATH, {
    ...pomAttributes,
    ...templatePomAttributes,
  });

  return pom;
};

const writeFrontendPlugin = async (pom: PomAsJson, templatePom: PomAsJson) => {
  const pomPlugins = get(pom, POM_PLUGINS_PATH, []);
  const templatePomPlugins = get(templatePom, POM_PLUGINS_PATH, []);

  // side effect
  set(pom, POM_PLUGINS_PATH, [...pomPlugins, ...templatePomPlugins]);

  return pom;
};

const writeBuildProfiles = async (pom: PomAsJson, templatePom: PomAsJson) => {
  const pomProfiles = get(pom, POM_PROFILES_PATH, []);
  const templatePomProfiles = get(templatePom, POM_PROFILES_PATH, []);

  // side effect
  set(pom, POM_PROFILES_PATH, [...pomProfiles, ...templatePomProfiles]);

  return pom;
};
