import { outdent } from 'outdent';
import { mocked } from 'jest-mock';
import path from 'path';

import { generateMavenConfiguration } from './maven';
import { atlasFeToolboxMock } from '../testing';
import { AtlasFeToolbox, PomAsJson } from '../types';

const POM_CONTENT_MOCK = outdent`
  <project xmlns="http://maven.apache.org/POM/4.0.0">
    <groupId>com.atlassian.tec</groupId>
    <artifactId>atlas-fe-refapp</artifactId>
    <properties>
      <refapp.version>5.0.0</refapp.version>
      <amps.version>8.10.4</amps.version>
    </properties>
    <dependencies>
    </dependencies>
    <build>
      <plugins>
        <plugin>
          <groupId>com.atlassian.maven.plugins</groupId>
          <artifactId>refapp-maven-plugin</artifactId>
          <version>\${amps.version}</version>
          <extensions>true</extensions>
          <configuration>
            <instructions>
              <Atlassian-Plugin-Key>\${atlassian.plugin.key}</Atlassian-Plugin-Key>
              <Export-Package>*</Export-Package>
              <Import-Package>*</Import-Package>
              <Spring-Context>*</Spring-Context>
            </instructions>
          </configuration>
        </plugin>
      </plugins>
    </build>
  </project>
`;

describe('maven utils', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPomContent: PomAsJson;

  beforeEach(() => {
    testingPomContent = atlasFeToolboxMock.pom.parseXml(POM_CONTENT_MOCK).content as PomAsJson;

    testingToolbox = {
      ...atlasFeToolboxMock,
      plugin: {
        // since we don't have a runtime, we need to manually set the right path to this plugin
        directory: path.resolve(__dirname, '..'),
        hidden: false,
        commands: [],
        extensions: [],
        defaults: {},
      },
      pom: {
        ...atlasFeToolboxMock.pom,
        getPomContent: async () => ({ content: testingPomContent, indent: '  ' }),
        writePomContent: async (content) => {
          testingPomContent = content;
        },
      },
    };
  });

  it('should print a message after done', async () => {
    await generateMavenConfiguration(testingToolbox);

    expect(testingToolbox.print.info).toHaveBeenCalledWith(
      `${testingToolbox.print.checkmark} Completed maven-frontend-plugin configuration`,
    );
  });

  it('should write the <Atlassian-Scan-Folders> configuration to the product maven plugin', async () => {
    const { content: pluginDeclaration } = testingToolbox.pom.parseXml(`
      <groupId>com.atlassian.maven.plugins</groupId>
      <artifactId>refapp-maven-plugin</artifactId>
      <version>\${amps.version}</version>
      <extensions>true</extensions>
      <configuration>
        <instructions>
          <Atlassian-Plugin-Key>\${atlassian.plugin.key}</Atlassian-Plugin-Key>
          <Export-Package>*</Export-Package>
          <Import-Package>*</Import-Package>
          <Spring-Context>*</Spring-Context>
          <Atlassian-Scan-Folders>META-INF/plugin-descriptors</Atlassian-Scan-Folders>
        </instructions>
      </configuration>
    `);
    const expectedDeclaration = {
      project: [
        {
          build: [
            {
              plugins: [
                {
                  plugin: expect.arrayContaining([expect.objectContaining(pluginDeclaration)]),
                },
              ],
            },
          ],
        },
      ],
    };

    await generateMavenConfiguration(testingToolbox);

    expect(testingPomContent).toMatchObject(expectedDeclaration);
  });

  it('should write the properties for the frontend plugin configuration', async () => {
    const { content: propertiesDeclaration } = testingToolbox.pom.parseXml(`
      <frontend.maven.plugin.version>1.12.1</frontend.maven.plugin.version>
      <frontend.maven.plugin.install.directory>\${project.build.directory}</frontend.maven.plugin.install.directory>
      <frontend.working.directory>\${project.basedir}</frontend.working.directory>
      <frontend.node.version>v20.15.1</frontend.node.version>
      <frontend.cmd.install>install</frontend.cmd.install>
      <frontend.cmd.build>run build</frontend.cmd.build>
      <frontend.cmd.watch.prepare>run start:watch-prepare</frontend.cmd.watch.prepare>
      <frontend.build.mode>production</frontend.build.mode>
    `);
    const expectedDeclaration = {
      project: [
        {
          properties: [propertiesDeclaration],
        },
      ],
    };

    await generateMavenConfiguration(testingToolbox);

    expect(testingPomContent).toMatchObject(expectedDeclaration);
  });

  it('should write the maven-frontend-plugin configuration', async () => {
    // If the plugin declaration is there, is safe to asume the execution statement was copied correctly
    const { content: pluginDeclaration } = testingToolbox.pom.parseXml(`
      <groupId>com.github.eirslett</groupId>
      <artifactId>frontend-maven-plugin</artifactId>
      <version>\${frontend.maven.plugin.version}</version>
      <configuration>
        <nodeVersion>\${frontend.node.version}</nodeVersion>
        <installDirectory>\${frontend.maven.plugin.install.directory}</installDirectory>
        <workingDirectory>\${frontend.working.directory}</workingDirectory>
        <environmentVariables>
            <NODE_ENV>\${frontend.build.mode}</NODE_ENV>
        </environmentVariables>
      </configuration>
    `);
    const expectedDeclaration = {
      project: [
        {
          build: [
            {
              plugins: [
                {
                  plugin: expect.arrayContaining([expect.objectContaining(pluginDeclaration)]),
                },
              ],
            },
          ],
        },
      ],
    };

    await generateMavenConfiguration(testingToolbox);

    expect(testingPomContent).toMatchObject(expectedDeclaration);
  });

  it('should write the frontend build profiles', async () => {
    // all these profiles should be present from the template
    const expectedProfiles = [
      expect.objectContaining({
        id: 'build-mode-is-set-by-node-env',
      }),
      expect.objectContaining({
        id: 'build',
        activation: [
          {
            property: [{ name: '!watchMode' }],
          },
        ],
      }),
      expect.objectContaining({
        id: 'watch-prepare',
        activation: [
          {
            property: [{ name: 'watchMode' }],
          },
        ],
      }),
    ];
    const expectedDeclaration = {
      project: [
        {
          profiles: [
            {
              profile: expectedProfiles,
            },
          ],
        },
      ],
    };

    await generateMavenConfiguration(testingToolbox);

    expect(testingPomContent).toMatchObject(expectedDeclaration);
  });

  it('should generate the configuration using Yarn if present instead of npm', async () => {
    // configuration using Yarn instead of NPM
    const expectedDeclaration = {
      project: [
        {
          build: [
            {
              plugins: [
                {
                  plugin: expect.arrayContaining([
                    expect.objectContaining({
                      groupId: 'com.github.eirslett',
                      artifactId: 'frontend-maven-plugin',
                      configuration: [
                        expect.objectContaining({
                          yarnVersion: '${frontend.yarn.version}',
                        }),
                      ],
                      executions: [
                        {
                          execution: expect.arrayContaining([
                            expect.objectContaining({
                              goals: [
                                {
                                  goal: 'yarn',
                                },
                              ],
                            }),
                          ]),
                        },
                      ],
                    }),
                  ]),
                },
              ],
            },
          ],
          properties: [
            expect.objectContaining({
              'frontend.yarn.version': 'v1.22.22',
            }),
          ],
        },
      ],
    };
    mocked(testingToolbox.packageManager.hasYarn).mockReturnValueOnce(true);

    await generateMavenConfiguration(testingToolbox);

    expect(testingPomContent).toMatchObject(expectedDeclaration);
  });
});
