import { PackageJSON } from 'gluegun/build/types/toolbox/meta-types';
import semver from 'semver';

import { AtlasFeToolbox } from '../types';

export const DEFAULT_PLUGINS = {
  '@atlassian/atlas-fe-typescript': 'TypeScript',
  '@atlassian/atlas-fe-react': 'React',
  '@atlassian/atlas-fe-clientside-extensions': 'Client-side Extensions',
};

export const installPlugins = async (toolbox: AtlasFeToolbox) => {
  const { plugins: pluginsToInstall } = toolbox.parameters.options;

  if (toolbox.config.isDevelopment) {
    await linkPackagesOnDevelopment(toolbox, pluginsToInstall);
  } else {
    await installPackages(toolbox, pluginsToInstall);
  }
};

const installPackages = async (toolbox: AtlasFeToolbox, packages: string[]) => {
  const spinner = toolbox.print.spin('Installing plugins...');
  const cliVersion = toolbox.meta.version();
  const runningPrerelease = !!semver.prerelease(toolbox.meta.version());
  // If we run a prerelease version (like snapshot or rc), install packages of that version. Otherwise, install latest.
  const packagesWithVersions = ['@atlassian/atlas-fe', ...packages].map((pkg) =>
    runningPrerelease ? `${pkg}@${cliVersion}` : `${pkg}@latest`,
  );

  try {
    // install plugin packages and core atlas-fe package
    await toolbox.packageManager.add(packagesWithVersions, { dev: true });

    // Load the recently installed plugins
    toolbox.plugins.load(packages);

    // done
    spinner.succeed(`Installed plugins.`);
  } catch (error) {
    spinner.fail('Failed to install plugins.');
    throw Error((error as Error).message);
  }
};

/**
 * TODO: THIS IS EXTREMLY HACKY. FIND A CLEANER WAY :)
 * When using a local verion of the CLI (linked from the repo instead of globally installed), we need to "link" instead of
 * "install" the plugins so that we can see our local changes in action. Linking doesn't install the dependencies of the plugins
 * (like eslint for the eslint plugin), so we also need to go through their dependencies and install them. This won't happen in
 * production since the packages installed come with their dependencies as well.
 */
const linkPackagesOnDevelopment = async (toolbox: AtlasFeToolbox, packages: string[]) => {
  const spinner = toolbox.print.spin('Linking plugins...');

  try {
    // link the plugin packages locally on development instead of installing from npm
    const packageManager = toolbox.packageManager.hasYarn() ? 'yarn' : 'npm';
    // link the core atlas-fe package
    await toolbox.system.run(`${packageManager} link @atlassian/atlas-fe`);

    // link plugins packages
    for (const pack of packages) {
      await toolbox.system.run(`${packageManager} link ${pack}`);
    }

    // Load the recently linked plugins
    toolbox.plugins.load(packages);

    // Yarn and NPM will only link the packages, but won't install their dependencies.
    // Some plugins need their dependencies to work on runtime (like ESLint). Let's install them.
    const plugins = toolbox.runtime?.plugins || [];

    for (const plugin of plugins) {
      const pluginDir = plugin.directory;

      if (!pluginDir) {
        continue;
      }

      // if this is the core plugin, we're running inside ./src/ instead of the root of the plugin, and that's not where the package.json file is.
      // So instead, we go look the package.json file one directory up.
      const packageJsonPath =
        plugin.name === 'atlas-fe'
          ? toolbox.filesystem.path(pluginDir, '..', 'package.json')
          : toolbox.filesystem.path(pluginDir, 'package.json');

      const rawPackageJson = await toolbox.filesystem.readAsync(packageJsonPath);

      if (!rawPackageJson) {
        continue;
      }

      const { dependencies }: PackageJSON = JSON.parse(rawPackageJson);

      const { packagesToInstall, packagesToLink } = Object.entries(dependencies || {}).reduce(
        (acc, [dependency, version]) => {
          // If a dependency has `atlas-fe` in its name, it's a plugin for the CLI and should be linked rather than installed.
          if (dependency.includes('atlas-fe')) {
            acc.packagesToLink.push(dependency);
          } else {
            // if not, then is just a regular dependency that needs to be installed
            acc.packagesToInstall.push(`${dependency}@${version}`);
          }

          return acc;
        },
        {
          packagesToInstall: [] as string[],
          packagesToLink: [] as string[],
        },
      );

      if (packagesToInstall.length) {
        await toolbox.packageManager.add(packagesToInstall, { dev: true });
      }

      for (const dependency of packagesToLink) {
        await toolbox.system.exec(`${packageManager} link ${dependency}`);
      }
    }

    spinner.warn(`Linked plugin packages on development mode.`);
  } catch (error) {
    spinner.fail('Failed to link plugins on development mode.');
    throw Error((error as Error).message);
  }
};
