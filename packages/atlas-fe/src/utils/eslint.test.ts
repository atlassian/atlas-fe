import { mocked } from 'jest-mock';

import { configureESLintPreset } from './eslint';
import { AtlasFeToolbox, PackageJSON } from '../types';
import { atlasFeToolboxMock } from '../testing';

describe('eslint utils - configureESLintPreset', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPackageJSON: PackageJSON;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      plugins: { ...atlasFeToolboxMock.plugins, exists: jest.fn() },
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
      },
    };

    testingPackageJSON = {
      name: 'testing-package',
      version: '0.0.1',
      description: 'testing package',
      scripts: {
        build: 'tsc .',
      },
    };

    mocked(testingToolbox.packageJson.write).mockImplementation(async (cb) => {
      const result = cb(testingPackageJSON);

      testingPackageJSON = result;

      return result;
    });
  });

  it(`should print a message after initializing ESLint configuration`, async () => {
    await configureESLintPreset(testingToolbox);

    expect(testingToolbox.print.info).toHaveBeenCalledWith(`${testingToolbox.print.checkmark} Generated ESLint configuration`);
  });

  it(`should write the javascript and prettier presets on the package.json file`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      eslintConfig: {
        root: true,
        extends: ['@atlassian/eslint-config-atlas-fe/javascript', '@atlassian/eslint-config-atlas-fe/prettier'],
        ignorePatterns: ['node_modules', 'target'],
      },
    };

    await configureESLintPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should write a script on package.json field to execute linting`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      scripts: {
        lint: `eslint . --fix`,
      },
    };

    await configureESLintPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should add the presets as dev dependencies to support eslint 8+`, async () => {
    await configureESLintPreset(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith('@atlassian/eslint-config-atlas-fe', { dev: true });
  });
});
