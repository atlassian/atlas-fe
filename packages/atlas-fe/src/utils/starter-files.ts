import { AtlasFeToolbox } from '../types';

export const generateStarterFiles = async (toolbox: AtlasFeToolbox) => {
  const { content } = await toolbox.pom.getPomContent();
  const { artifactId } = content.project[0];
  const { pluginKey, plugins } = toolbox.parameters.options;

  // Generate started config file
  await toolbox.template.generate({
    template: 'atlas-fe.config.js.ejs',
    target: 'atlas-fe.config.js',
    // pass the global JSON object to the template so that it can format the plugins list
    props: { pluginKey, artifactId, plugins, JSON },
  });

  // Generate started entrypoint file
  await toolbox.template.generate({
    template: 'src/frontend/entrypoints/atl-general.ejs',
    target: `src/frontend/entrypoints/atl-general.js`,
  });

  // Generate a .nvmrc file for the plugin
  await toolbox.template.generate({
    template: '.nvmrc',
    target: '.nvmrc',
  });

  toolbox.print.info(`${toolbox.print.checkmark} Generated configuration file`);
};
