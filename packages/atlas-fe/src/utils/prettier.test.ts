import { mocked } from 'jest-mock';

import { configurePrettierConfig } from './prettier';
import { AtlasFeToolbox, PackageJSON } from '../types';
import { atlasFeToolboxMock } from '../testing';

describe('prettier utils - configurePrettierConfig', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPackageJSON: PackageJSON;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      plugins: { ...atlasFeToolboxMock.plugins, exists: jest.fn() },
      plugin: atlasFeToolboxMock.plugin,
      filesystem: {
        ...atlasFeToolboxMock.filesystem,
        path: jest.fn(),
      },
    };

    testingPackageJSON = {
      name: 'testing-package',
      version: '0.0.1',
      description: 'testing package',
      scripts: {
        build: 'tsc .',
      },
    };

    mocked(testingToolbox.packageJson.write).mockImplementation(async (cb) => {
      const result = cb(testingPackageJSON);

      testingPackageJSON = result;

      return result;
    });
  });

  it(`should write the prettier preset on the package.json file`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      prettier: '@atlassian/atlas-fe/presets/prettier',
    };

    await configurePrettierConfig(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should write a script on package.json field to execute formatting`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      scripts: {
        format: `prettier . --write`,
      },
    };

    mocked(testingToolbox.plugins.exists).mockReturnValue(true);

    await configurePrettierConfig(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should generate a .editorconfig file`, async () => {
    const expectedTemplateConfig = {
      template: '.editorconfig.ejs',
      target: '.editorconfig',
    };
    testingToolbox.plugin = {
      directory: '../..',
      hidden: false,
      commands: [],
      extensions: [],
      defaults: {},
    };
    mocked(testingToolbox.filesystem.path).mockImplementation((...args) => args.join('/'));

    await configurePrettierConfig(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(expectedTemplateConfig);
  });

  it(`should generate a .prettierignore file`, async () => {
    const expectedTemplateConfig = {
      template: '.prettierignore.ejs',
      target: '.prettierignore',
    };
    testingToolbox.plugin = {
      directory: '../..',
      hidden: false,
      commands: [],
      extensions: [],
      defaults: {},
    };
    mocked(testingToolbox.filesystem.path).mockImplementation((...args) => args.join('/'));

    await configurePrettierConfig(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(expectedTemplateConfig);
  });
});
