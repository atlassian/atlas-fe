import { AtlasFeToolbox } from '../types';

export const configureBabelPreset = async (toolbox: AtlasFeToolbox) => {
  await toolbox.packageJson.write((packageJsonContent) => {
    const presets = ['module:@atlassian/atlas-fe/presets/babel'];

    packageJsonContent['babel'] = { presets };

    packageJsonContent['browserslist'] = ['extends @atlassian/browserslist-config-server'];

    return packageJsonContent;
  });
  toolbox.print.info(`${toolbox.print.checkmark} Generated Babel configuration`);
};

export const chainBabelLoader = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // prettier-ignore
    {
      webpackConfig
      .module
        .rule('babel')
          .test(/\.(js|jsx|ts|tsx)$/)
            .exclude.add(/node_modules/).end()
          .use('babel')
            .loader('babel-loader').end()

        webpackConfig
          .resolve
          .extensions.add('.js').end();
    }
  });
};
