import { mocked } from 'jest-mock';

import { installPlugins, DEFAULT_PLUGINS } from './plugins';
import { atlasFeToolboxMock, TestingReadAsync } from '../testing';
import { AtlasFeToolbox } from '../types';

interface TestingRuntime {
  plugins: { directory: string }[];
}

describe('plugins utils', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      config: {
        ...atlasFeToolboxMock.config,
        isDevelopment: false,
      },
      parameters: {
        options: {
          plugins: [...Object.keys(DEFAULT_PLUGINS)],
        },
      },
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
        hasYarn: jest.fn(() => false),
      },
      system: {
        ...atlasFeToolboxMock.system,
        run: jest.fn(),
        exec: jest.fn(),
      },
      filesystem: {
        ...atlasFeToolboxMock.filesystem,
      },
    };

    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [],
    };

    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve(''));
  });

  it(`should install all packages if running in production mode`, async () => {
    const expectedPlugins = testingToolbox.parameters.options.plugins;
    const expectedPackages = ['@atlassian/atlas-fe@latest', ...expectedPlugins.map((pkg: string) => `${pkg}@latest`)];

    await installPlugins(testingToolbox);

    expect(testingToolbox.config.isDevelopment).toBe(false);
    expect(testingToolbox.print.spin).toHaveBeenCalledWith('Installing plugins...');
    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith(expectedPackages, { dev: true });
    expect(testingToolbox.plugins.load).toHaveBeenCalledWith(expectedPlugins);
  });

  it('should throw error if there was a problem installing the plugins', async () => {
    mocked(testingToolbox.packageManager.add).mockImplementationOnce(() => {
      throw Error('testing error');
    });
    expect(installPlugins(testingToolbox)).rejects.toThrowError('testing error');
  });

  it('should link the plugin packages locally instead of installing when running the CLI on development mode', async () => {
    // enable development mode
    testingToolbox.config.isDevelopment = true;

    await installPlugins(testingToolbox);

    expect(testingToolbox.print.spin).toHaveBeenCalledWith('Linking plugins...');
    Object.keys(DEFAULT_PLUGINS).forEach((pack) => expect(testingToolbox.system.run).toHaveBeenCalledWith(`npm link ${pack}`));
    expect(testingToolbox.plugins.load).toHaveBeenCalledWith(Object.keys(DEFAULT_PLUGINS));
  });

  it('should install all the third party dependencies of the linked packages when running on development mode', async () => {
    // enable development mode
    testingToolbox.config.isDevelopment = true;
    // we need to trick TS here in order to push read-only values to the runtime, because we don't really have a runtime
    (testingToolbox.runtime as TestingRuntime).plugins = [{ directory: './fake-plugin-directory' }];
    // use a fake package.json to test the installation of dependencies
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve(`{
        "dependencies": {
          "fake-dependency": "^1.2.3"
        }
      }`),
    );

    await installPlugins(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith(['fake-dependency@^1.2.3'], { dev: true });
  });

  it('should link all the internal "atlas-fe" dependencies of the linked packages when running on development mode', async () => {
    // enable development mode
    testingToolbox.config.isDevelopment = true;
    // we need to trick TS here in order to push read-only values to the runtime, because we don't really have a runtime
    (testingToolbox.runtime as TestingRuntime).plugins = [{ directory: './fake-plugin-directory' }];
    // use a fake package.json to test the installation of dependencies
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve(`{
        "dependencies": {
          "atlas-fe-fake-dependency": "^1.2.3"
        }
      }`),
    );

    await installPlugins(testingToolbox);

    expect(testingToolbox.system.exec).toHaveBeenCalledWith('npm link atlas-fe-fake-dependency');
  });
});
