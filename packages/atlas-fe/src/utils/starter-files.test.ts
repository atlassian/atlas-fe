import { mocked } from 'jest-mock';

import { generateStarterFiles } from './starter-files';
import { atlasFeToolboxMock } from '../testing';
import { AtlasFeToolbox } from '../types';

describe('starter-files utils', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      pom: {
        ...atlasFeToolboxMock.pom,
        getPomContent: jest.fn(() =>
          Promise.resolve({
            content: {
              project: [
                {
                  groupId: 'com.atlassian.atlas-fe',
                  artifactId: 'atlas-fe-testing',
                },
              ],
            },
            indent: '  ',
          }),
        ),
      },
      parameters: {
        options: {
          pluginKey: 'com.atlassian.atlas-fe.atlas-fe-testing',
          plugins: [],
        },
      },
      template: {
        ...atlasFeToolboxMock.template,
        generate: jest.fn(),
      },
      plugins: {
        ...atlasFeToolboxMock.plugins,
        exists: jest.fn(() => false),
      },
    };
  });

  it('should print a message after done generating the starter files', async () => {
    await generateStarterFiles(testingToolbox);

    expect(testingToolbox.print.info).toHaveBeenCalledWith(`${testingToolbox.print.checkmark} Generated configuration file`);
  });

  it('should grab the template files from the template folder', async () => {
    await generateStarterFiles(testingToolbox);

    mocked(testingToolbox.template.generate).mock.calls.forEach(([{ template: templateName }]) => {
      const filePath = testingToolbox.filesystem.path(__dirname, `../templates/${templateName}`);

      expect(testingToolbox.filesystem.exists(filePath)).toBe('file');
    });
  });

  it('should generate a configuration file with the pluginKey and artifactId', async () => {
    await generateStarterFiles(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(
      expect.objectContaining({
        target: 'atlas-fe.config.js',
        props: {
          JSON: expect.anything(),
          plugins: [],
          pluginKey: 'com.atlassian.atlas-fe.atlas-fe-testing',
          artifactId: 'atlas-fe-testing',
        },
      }),
    );
  });

  it('should generate a starter entry point file', async () => {
    await generateStarterFiles(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(
      expect.objectContaining({
        target: `src/frontend/entrypoints/atl-general.js`,
      }),
    );
  });
});
