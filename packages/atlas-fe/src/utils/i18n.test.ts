import TerserPlugin from 'terser-webpack-plugin';

import { chainI18n } from './i18n';
import { atlasFeToolboxMock, chainWrmPluginMock } from '../testing';
import { AtlasFeToolbox } from '../types';

const expectedProvidedDependencies = {
  'wrm/i18n': {
    dependency: 'com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n',
    import: {
      var: 'require("wrm/i18n")',
      amd: 'wrm/i18n',
    },
  },
};

describe('i18n utils', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      webpackConfig.optimization.clear();
      webpackConfig.plugins.clear();
    });

    // we need WrmPlugin to be present in the configuration to add provided dependencies
    chainWrmPluginMock(testingToolbox);

    // we also need the Terser plugin in order to tap its configuration
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      webpackConfig.optimization.minimizer('terser').use(TerserPlugin);
    });
  });

  it(`should configure the i18n provided dependencies when watch is disabled`, () => {
    chainI18n(testingToolbox);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        {
          pluginKey: 'fake-plugin',
          providedDependencies: expectedProvidedDependencies,
        },
      ]);
    });
  });

  it(`should configure the i18n provided dependencies when watch is enabled`, () => {
    chainI18n(testingToolbox, true);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        {
          pluginKey: 'fake-plugin',
          providedDependencies: expectedProvidedDependencies,
        },
      ]);
    });
  });

  it(`should prevent Terser minimizing the usage of I18n.getText when watch is disabled`, () => {
    chainI18n(testingToolbox);

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.optimization.minimizer('terser').entries().args[0].terserOptions).toMatchObject({
        mangle: {
          reserved: ['I18n', 'getText'],
        },
      });
    });
  });

  it(`should configure i18n properties loader when watch is enabled`, () => {
    chainI18n(testingToolbox);

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.module.rule('i18n')).toBeTruthy();
    });
  });
});
