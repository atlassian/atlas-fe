import { AtlasFeToolbox } from '../types';
import { DEFAULT_PLUGINS } from './plugins';

export const askInitQuestions = async (toolbox: AtlasFeToolbox) => {
  const pluginKey = await askPluginKey(toolbox);

  const plugins = await askPluginsToInstall(toolbox);

  toolbox.parameters.options = { ...toolbox.parameters.options, pluginKey, plugins };
};

const askPluginKey = async (toolbox: AtlasFeToolbox) => {
  let { yes: useDefaultPluginKey } = toolbox.parameters.options;
  const defaultPluginKey = await toolbox.pom.getPluginKeyFromPom();

  // don't ask if the --yes parameter is used
  if (useDefaultPluginKey) {
    toolbox.print.warning(`using plugin key: ${defaultPluginKey}`);

    return defaultPluginKey;
  }

  useDefaultPluginKey = await toolbox.prompt.confirm(
    `Is the value of the plugin key correct? You can find the plugin key inside the 'atlassian-plugin.xml' file.
  ${defaultPluginKey}`,
    true,
  );

  if (useDefaultPluginKey) {
    return defaultPluginKey;
  }

  const { userPluginKey } = await toolbox.prompt.ask<{ userPluginKey: string }>({
    name: 'userPluginKey',
    type: 'input',
    message: 'Type your plugin key',
  });

  return userPluginKey;
};

const askPluginsToInstall = async (toolbox: AtlasFeToolbox) => {
  let { yes: useDefaultPlugins } = toolbox.parameters.options;
  const defaultPlugins = [...Object.keys(DEFAULT_PLUGINS)];

  // don't ask if the --yes parameter is used
  if (useDefaultPlugins) {
    toolbox.print.warning(`using recommended "Atlassian" setup`);
    return defaultPlugins;
  }

  useDefaultPlugins = await toolbox.prompt.confirm(`Would you like to use the recommended setup by Atlassian?`, true);

  if (useDefaultPlugins) {
    return defaultPlugins;
  }

  const { userPlugins } = await toolbox.prompt.ask<{ userPlugins: string[] }>([
    {
      name: 'userPlugins',
      message: 'Select the configurations you want to use for this project (use space to toggle options)',
      type: 'multiselect',
      choices: Object.entries(DEFAULT_PLUGINS).reduce(
        (acc, [name, message]) => [...acc, { name, message }],
        [] as { name: string; message: string }[],
      ),
    },
  ]);

  return userPlugins;
};
