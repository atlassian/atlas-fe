import { mocked } from 'jest-mock';

import { DEFAULT_PLUGINS } from './plugins';
import { askInitQuestions } from './init-questions';
import { atlasFeToolboxMock } from '../testing';
import { AtlasFeToolbox } from '../types';

describe('init-questions utils', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      pom: {
        ...atlasFeToolboxMock.pom,
        getPluginKeyFromPom: jest.fn(async () => 'com.atlassian.atlas-fe.atlas-fe-testing'),
      },
      parameters: {
        options: {},
      },
      prompt: {
        ...atlasFeToolboxMock.prompt,
        ask: jest.fn(),
        confirm: jest.fn(),
      },
    };
  });

  describe('plugin key', () => {
    it(`should set the default plugin key if '--yes' option is used`, async () => {
      const expectedPluginKey = 'com.atlassian.atlas-fe.atlas-fe-testing';
      testingToolbox.parameters.options.yes = true;

      await askInitQuestions(testingToolbox);

      expect(testingToolbox.parameters.options.pluginKey).toBe(expectedPluginKey);
    });

    it('should set the default pluign key if confirmed plugin key is correct', async () => {
      const expectedPluginKey = 'com.atlassian.atlas-fe.atlas-fe-testing';
      mocked(testingToolbox.prompt.confirm).mockResolvedValue(true);

      await askInitQuestions(testingToolbox);

      // make sure `--yes` is not being used
      expect(testingToolbox.parameters.options.yes).toBe(undefined);
      expect(testingToolbox.parameters.options.pluginKey).toBe(expectedPluginKey);
    });

    it('should set a custom plugin key if specified', async () => {
      const expectedPluginKey = 'com.atlassian.atlas-fe.atlas-fe-custom';
      mocked(testingToolbox.prompt.confirm).mockResolvedValue(false);
      mocked(testingToolbox.prompt.ask).mockResolvedValue({ userPluginKey: expectedPluginKey });

      await askInitQuestions(testingToolbox);

      // make sure `--yes` is not being used
      expect(testingToolbox.parameters.options.yes).toBe(undefined);
      expect(testingToolbox.parameters.options.pluginKey).toBe(expectedPluginKey);
    });
  });

  describe('plugins to install', () => {
    it(`should use the default list of plugins if '--yes' option is used`, async () => {
      const expectedPluginList = [...Object.keys(DEFAULT_PLUGINS)];
      testingToolbox.parameters.options.yes = true;

      await askInitQuestions(testingToolbox);

      expect(testingToolbox.parameters.options.plugins).toMatchObject(expectedPluginList);
    });

    it('should use the default list of plugins if "Use the recommended setup by Atlassian" is chosen', async () => {
      const expectedPluginList = [...Object.keys(DEFAULT_PLUGINS)];
      mocked(testingToolbox.prompt.confirm).mockResolvedValue(true);

      await askInitQuestions(testingToolbox);

      // make sure `--yes` is not being used
      expect(testingToolbox.parameters.options.yes).toBe(undefined);
      expect(testingToolbox.parameters.options.plugins).toMatchObject(expectedPluginList);
    });

    it('should use the custom list of plugins by the user', async () => {
      const expectedPluginList = ['@atlassian/atlas-fe-testing-plugin'];
      mocked(testingToolbox.prompt.confirm).mockResolvedValue(false);
      mocked(testingToolbox.prompt.ask).mockResolvedValue({ userPlugins: expectedPluginList });

      await askInitQuestions(testingToolbox);

      // make sure `--yes` is not being used
      expect(testingToolbox.parameters.options.yes).toBe(undefined);
      expect(testingToolbox.parameters.options.plugins).toMatchObject(expectedPluginList);
    });
  });
});
