import { AtlasFeToolbox } from '../types';

/**
 * Configures webpack to work with Atlassian properties files (translations), both in development and production
 */
export const chainI18n = (toolbox: AtlasFeToolbox, watch = false) => {
  configureI18nProvidedDependencies(toolbox);

  if (watch) {
    chainI18nLoaderForDevServer(toolbox);
  } else {
    tapTerserPluginWithI18n(toolbox);
  }
};

/**
 * We make use of `wrm/i18n` available from WRM
 *
 * https://bitbucket.org/atlassian/atlassian-plugins-webresource/src/master/UPGRADE_420.md
 */
const configureI18nProvidedDependencies = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.addWrmProvidedDependencies({
    'wrm/i18n': {
      dependency: 'com.atlassian.plugins.atlassian-plugins-webresource-plugin:i18n',
      import: {
        var: 'require("wrm/i18n")',
        amd: 'wrm/i18n',
      },
    },
  });
};

/**
 * Use i18n-loader in order to get translations working with dev-server
 *
 * https://www.npmjs.com/package/@atlassian/i18n-properties-loader#user-content-usage-example
 */
const chainI18nLoaderForDevServer = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    // prettier-ignore
    {
      webpackConfig.module
        .rule('i18n')
          .test(/\.(js|jsx|ts|tsx)$/)
          .use('i18n')
            .loader('@atlassian/i18n-properties-loader')
              .options({ i18nFiles: toolbox.config.i18nFiles ?? [] });
    }
  });
};

/**
 * WRM translation transformer replaces any usage of `I18n.getText` in the code when serving the assets, so we need
 * to tell webpack not to minify the usage of the function.
 *
 * https://www.npmjs.com/package/@atlassian/wrm-react-i18n#user-content-minification-of-production-bundle
 */
const tapTerserPluginWithI18n = (toolbox: AtlasFeToolbox) => {
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    webpackConfig.optimization
      // terser is already configured for production in ../utils/webpack, so all we need to do is to tap the configuration
      .minimizer('terser')
      .tap((args) => [
        ...args,
        {
          terserOptions: {
            mangle: {
              // Don't mangle usage of I18n.getText() function
              reserved: ['I18n', 'getText'],
            },
          },
        },
      ]);
  });
};
