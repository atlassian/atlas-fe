import { mocked } from 'jest-mock';

import { configureBabelPreset, chainBabelLoader } from './babel';
import { atlasFeToolboxMock } from '../testing';
import { AtlasFeToolbox, PackageJSON } from '../types';

describe('babel utils - configureBabelPreset', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPackageJSON: PackageJSON;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    testingPackageJSON = {
      name: 'testing-package',
      version: '0.0.1',
      description: 'testing package',
      scripts: {
        build: 'tsc .',
      },
    };

    mocked(testingToolbox.packageJson.write).mockImplementation(async (cb) => {
      const result = cb(testingPackageJSON);

      testingPackageJSON = result;

      return result;
    });
  });

  it(`should write the browserslist preset on the package.json file`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      babel: {
        presets: ['module:@atlassian/atlas-fe/presets/babel'],
      },
    };

    await configureBabelPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should write the javascript preset on the package.json file`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      babel: {
        presets: ['module:@atlassian/atlas-fe/presets/babel'],
      },
    };

    await configureBabelPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it(`should print a message after finishing writting babel configuration`, async () => {
    await configureBabelPreset(testingToolbox);

    expect(testingToolbox.print.info).toHaveBeenCalledWith(`${testingToolbox.print.checkmark} Generated Babel configuration`);
  });
});

describe('babel utils - chainBabelLoader', () => {
  let testingToolbox: AtlasFeToolbox;
  const expectedModuleDefinition = {
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: [/node_modules/],
          use: [{ loader: 'babel-loader' }],
        },
      ],
    },
  };

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock, plugins: { exists: jest.fn(() => false), run: jest.fn(), load: jest.fn() } };

    // clear all webpack config
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      webpackConfig.module.rules.clear();
      webpackConfig.resolve.extensions.clear();
    });
  });

  it(`should configure webpack with babel-loader for JavaScript`, () => {
    const expectedWebpackConfig = {
      ...expectedModuleDefinition,
      resolve: {
        extensions: ['.js'],
      },
    };

    chainBabelLoader(testingToolbox);

    expect(testingToolbox.webpack.resolveWebpackConfig()).toEqual(expectedWebpackConfig);
  });
});
