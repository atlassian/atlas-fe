import { AtlasFeToolbox } from '../types';

export const configureESLintPreset = async (toolbox: AtlasFeToolbox) => {
  await toolbox.packageJson.write((packageContent) => {
    const presets = [
      '@atlassian/eslint-config-atlas-fe/javascript',
      // prettier rules must be applied after all other configurations
      '@atlassian/eslint-config-atlas-fe/prettier',
    ].filter(Boolean);

    packageContent['eslintConfig'] = {
      root: true,
      extends: presets,
      ignorePatterns: ['node_modules', 'target'],
    };

    packageContent['scripts'] = {
      ...packageContent['scripts'],
      lint: `eslint . --fix`,
    };

    return packageContent;
  });

  await toolbox.packageManager.add('@atlassian/eslint-config-atlas-fe', { dev: true });

  toolbox.print.info(`${toolbox.print.checkmark} Generated ESLint configuration`);
};
