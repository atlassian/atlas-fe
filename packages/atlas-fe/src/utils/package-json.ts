import { AtlasFeToolbox } from '../types';

export const initPackageJson = async (toolbox: AtlasFeToolbox) => {
  const packageJsonExists = await toolbox.filesystem.existsAsync('package.json');

  // create a fresh package.json file if none exists
  if (!packageJsonExists) {
    toolbox.print.warning('package.json not found. Creating one...');
    await createPackageJson(toolbox);
  } else {
    toolbox.print.warning('package.json found. Modifiying...');
  }

  await toolbox.packageJson.write((packageJsonContent) => {
    packageJsonContent['scripts'] = {
      build: 'atlas-fe build',
      start: 'atlas-fe start',
      'start:watch': 'atlas-fe watch',
      'start:watch-prepare': 'atlas-fe watch-prepare',
      'start:prepare': 'atlas-debug -U -DwatchMode -DskipTests',
    };

    return packageJsonContent;
  });
};

const createPackageJson = async (toolbox: AtlasFeToolbox) => {
  let { name } = toolbox.parameters.options;

  if (name) {
    toolbox.print.warning(`creating package with name: ${name}`);
  } else {
    name = await toolbox.pom.getPluginKeyFromPom();
  }

  await toolbox.template.generate({
    template: 'package.json.ejs',
    target: 'package.json',
    props: { name },
  });
};
