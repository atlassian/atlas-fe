import TerserPlugin from 'terser-webpack-plugin';

import { chainBaseWebpackConfig, chainUserWebpackConfig } from './webpack';
import { atlasFeToolboxMock, chainWrmPluginMock } from '../testing';
import { AtlasFeToolbox } from '../types';

describe('webpack utils', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      config: {
        ...atlasFeToolboxMock.config,
        pluginKey: 'testing-plugin',
        entries: {
          'testing-1-js': './testing-1.js',
          'testing-2-js': './testing-2.js',
        },
        providedDependencies: {},
      },
      webpack: {
        ...atlasFeToolboxMock.webpack,
      },
    };

    // clean up webpack configuration before testing
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      webpackConfig.entryPoints.clear();
      webpackConfig.output.clear();
      webpackConfig.resolve.clear();
      webpackConfig.mode('none');
      webpackConfig.plugins.clear();
      webpackConfig.optimization.clear();
      webpackConfig.devtool(false);
    });

    process.env.NODE_ENV = 'test';
  });

  it('should chain the base development configuration', () => {
    chainBaseWebpackConfig(testingToolbox);

    const result = testingToolbox.webpack.resolveWebpackConfig();

    expect(result.entry).toEqual({
      'testing-1-js': ['./testing-1.js'],
      'testing-2-js': ['./testing-2.js'],
    });
    expect(result.mode).toBe('development');
    expect(result.output).toEqual({ path: '/mock/output', filename: '[name].bundle.js' });
    expect(result.resolve).toEqual({ extensions: ['.js', '*'] });
    expect(result.optimization).toEqual({
      minimizer: [expect.any(TerserPlugin)],
      minimize: false,
      runtimeChunk: false,
      splitChunks: false,
    });
    expect(result.devtool).toBe('cheap-source-map');

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        {
          pluginKey: 'testing-plugin',
          xmlDescriptors: '/mock/output/META-INF/plugin-descriptors/wr-webpack-bundles.xml',
          watch: false,
          watchPrepare: false,
          providedDependencies: {},
        },
      ]);
    });
  });

  it('should chain the base production configuration', () => {
    process.env.NODE_ENV = 'production';

    chainBaseWebpackConfig(testingToolbox);

    const result = testingToolbox.webpack.resolveWebpackConfig();

    expect(result.entry).toEqual({
      'testing-1-js': ['./testing-1.js'],
      'testing-2-js': ['./testing-2.js'],
    });
    expect(result.mode).toBe('production');
    expect(result.output).toEqual({ path: '/mock/output', filename: '[name].bundle.js' });
    expect(result.resolve).toEqual({ extensions: ['.js', '*'] });
    expect(result.optimization).toEqual({
      minimizer: [expect.any(TerserPlugin)],
      minimize: true,
      runtimeChunk: false,
      splitChunks: {
        chunks: 'all',
        maxInitialRequests: Infinity,
        minSize: 0,
      },
    });
    expect(result.devtool).toBe(false);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        {
          pluginKey: 'testing-plugin',
          xmlDescriptors: '/mock/output/META-INF/plugin-descriptors/wr-webpack-bundles.xml',
          watch: false,
          watchPrepare: false,
          providedDependencies: {},
        },
      ]);
    });
  });

  it('should chain the base watch configuration', () => {
    chainBaseWebpackConfig(testingToolbox, { shouldWatch: true });

    const result = testingToolbox.webpack.resolveWebpackConfig();

    expect(result.output).toEqual({
      publicPath: 'http://127.0.0.1:3333/',
      path: '/mock/output',
      filename: '[name].js',
      chunkFilename: '[name].chunk.js',
    });
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        expect.objectContaining({
          watch: true,
          watchPrepare: false,
        }),
      ]);
    });
  });

  it('should chain the base watch-prepare configuration', () => {
    chainBaseWebpackConfig(testingToolbox, { shouldWatch: true, shouldPrepare: true });

    const result = testingToolbox.webpack.resolveWebpackConfig();

    expect(result.output).toEqual({
      publicPath: 'http://127.0.0.1:3333/',
      path: '/mock/output',
      filename: '[name].js',
      chunkFilename: '[name].chunk.js',
    });
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        expect.objectContaining({
          watch: true,
          watchPrepare: true,
        }),
      ]);
    });
  });

  it('should chain the user webpack configuration', () => {
    class FakePlugin {
      constructor(private options: Record<string, unknown>) {}

      apply(compiler: unknown) {
        return compiler;
      }
    }
    testingToolbox.config.chainWebpackConfig = jest.fn((webpackConfig) => {
      webpackConfig.entry('user-entry-point').add('./user-entry-point.js');
      webpackConfig.plugin('FakePlugin').use(FakePlugin, [
        {
          option: 'value',
        },
      ]);
    });

    // WrmPlugin needs to be present in the webpack configuration. We can use the testing WrmPlugin this time
    chainWrmPluginMock(testingToolbox);

    chainUserWebpackConfig(testingToolbox, 'start');

    const result = testingToolbox.webpack.resolveWebpackConfig();

    expect(result.entry).toEqual({
      'user-entry-point': ['./user-entry-point.js'],
    });

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('FakePlugin').entries().args).toEqual([
        {
          option: 'value',
        },
      ]);

      expect(testingToolbox.config.chainWebpackConfig).toHaveBeenCalledWith(webpackConfig, 'start');
    });
  });

  it('should add the user provided dependencies', () => {
    const testingProvidedDependencies = {
      'dependency-name': {
        dependency: 'atlassian.plugin.key:webresource-key',
        import: {
          var: "require('dependency-amd-module-name')",
          amd: 'dependency-amd-module-name',
        },
      },
    };
    testingToolbox.config.providedDependencies = testingProvidedDependencies;

    // WrmPlugin needs to be present in the webpack configuration.
    chainBaseWebpackConfig(testingToolbox);

    chainUserWebpackConfig(testingToolbox, 'start');

    expect(testingToolbox.print.info).toHaveBeenCalledWith(`Applying webpack configuration from atlas-fe.config.js`);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      expect(webpackConfig.plugin('WrmPlugin').entries().args).toEqual([
        expect.objectContaining({
          providedDependencies: testingProvidedDependencies,
        }),
      ]);
    });
  });
});
