import { AtlasFeToolbox } from '../types';

export const configurePrettierConfig = async (toolbox: AtlasFeToolbox) => {
  await toolbox.packageJson.write((packageContent) => {
    packageContent['prettier'] = '@atlassian/atlas-fe/presets/prettier';

    packageContent['scripts'] = {
      ...packageContent['scripts'],
      format: `prettier . --write`,
    };

    return packageContent;
  });

  await toolbox.template.generate({
    template: '.editorconfig.ejs',
    target: '.editorconfig',
  });

  await toolbox.template.generate({
    template: '.prettierignore.ejs',
    target: '.prettierignore',
  });

  toolbox.print.info(`${toolbox.print.checkmark} Generated Prettier configuration`);
};
