import { mocked } from 'jest-mock';
import path from 'path';

import { initPackageJson } from './package-json';
import { atlasFeToolboxMock } from '../testing';
import { AtlasFeToolbox, PackageJSON } from '../types';

describe('package-json utils', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPackageJSON: PackageJSON;

  beforeEach(() => {
    testingPackageJSON = {};

    // re-create testing toolbox with clean APIs
    testingToolbox = {
      ...atlasFeToolboxMock,
      plugin: {
        // since we don't have a runtime, we need to manually set the right path to this plugin
        directory: path.resolve(__dirname, '..'),
        hidden: false,
        commands: [],
        extensions: [],
        defaults: {},
      },
      filesystem: {
        ...atlasFeToolboxMock.filesystem,
        existsAsync: jest.fn(() => Promise.resolve(false)),
      },
      packageJson: {
        ...atlasFeToolboxMock.packageJson,
        write: jest.fn((cb) => {
          const newContent = cb({ ...testingPackageJSON });
          testingPackageJSON = newContent;
          return Promise.resolve(newContent);
        }),
      },
      template: {
        ...atlasFeToolboxMock.template,
        generate: jest.fn(({ props }) => {
          // testing template
          testingPackageJSON = { version: '0.0.0', name: props?.name };
          // Just needs to return a string. We don't really care what it is since we read the package.json using another utility
          // (packageJson.write) instead of the result of this call
          return Promise.resolve('');
        }),
      },
      parameters: {
        ...atlasFeToolboxMock.parameters,
        options: {},
      },
    };
  });

  it('should write the existing package.json file with the atlas-fe commands', async () => {
    mocked(testingToolbox.filesystem.existsAsync).mockReturnValueOnce(Promise.resolve('file'));
    testingPackageJSON = {
      version: '0.0.0',
      name: 'testing-package',
    };

    await initPackageJson(testingToolbox);

    expect(testingToolbox.packageJson.write).toHaveBeenCalled();

    expect(testingPackageJSON).toMatchObject({
      version: '0.0.0',
      name: 'testing-package',
      scripts: {
        build: 'atlas-fe build',
        start: 'atlas-fe start',
        'start:watch': 'atlas-fe watch',
        'start:watch-prepare': 'atlas-fe watch-prepare',
        'start:prepare': 'atlas-debug -U -DwatchMode -DskipTests',
      },
    });
  });

  it('should generate a new package.json if none exists, and write all the atlas-fe commands', async () => {
    // setting name through CLI options
    testingToolbox.parameters.options = { name: 'testing-package' };

    // package.json testing content should be empty (meaning, the file doesn't exists) before the initialization
    expect(testingPackageJSON).toEqual({});

    await initPackageJson(testingToolbox);

    expect(testingPackageJSON).toEqual(
      expect.objectContaining({
        version: '0.0.0',
        name: 'testing-package',
        scripts: expect.anything(),
      }),
    );
    expect(testingToolbox.print.warning).toHaveBeenCalledWith(`creating package with name: testing-package`);
  });

  it('should take the plugin key from the pom to use it as the name for package.json file if none is passed as CLI option', async () => {
    const expectedPluginKey = 'com.atlassian.example.plugin-key';
    mocked(testingToolbox.pom.getPluginKeyFromPom).mockReturnValueOnce(Promise.resolve(expectedPluginKey));

    // package.json testing content should be empty (meaning, the file doesn't exists) before the initialization
    expect(testingPackageJSON).toEqual({});
    // no options set on CLI parameters
    expect(testingToolbox.parameters.options).toEqual({});

    await initPackageJson(testingToolbox);

    expect(testingPackageJSON).toEqual(
      expect.objectContaining({
        name: expectedPluginKey,
        version: '0.0.0',
        scripts: expect.anything(),
      }),
    );
  });
});
