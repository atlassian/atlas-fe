import { build, GluegunToolbox, print, semver } from 'gluegun';
import { Options } from 'gluegun/build/types/domain/options';
import Amplitude from 'amplitude';
import fs from 'fs';
import path from 'path';

import { AtlasFeToolbox, PackageJSON } from './types';
import { AMP_CLIENT_ID } from './constants';

async function trackExecutedCommand(toolbox: AtlasFeToolbox) {
  try {
    // don't track events if is running as development
    if (toolbox.config.isDevelopment) {
      return;
    }

    const ampClient = new Amplitude(AMP_CLIENT_ID);
    const event = {
      event_type: 'command-executed',
      device_id: 'required-but-not-used',
      event_properties: {
        command: toolbox.command?.name,
        intalledPlugins: (toolbox.runtime?.plugins || []).map((plugin) => plugin.name),
        version: toolbox.meta.version(),
      },
    };

    await ampClient.track(event);
  } catch (error) {
    // no need to handle this error from analytics
  }
}

function verifyNodeVersion() {
  try {
    // Do not use `import packageJson from "../package.json"`. It will mess up with the structure of the code inside `build`
    const packageJson: PackageJSON = JSON.parse(fs.readFileSync(path.resolve(__dirname, '..', 'package.json')).toString());
    const version = packageJson.engines?.node || '';

    if (!semver.satisfies(process.version, version)) {
      print.error(`Required node version ${version} not satisfied with current version ${process.version}.`);
      process.exit(1);
    }
  } catch (error) {
    print.error('Unable to find package.json file in this directory. Aborting...');
  }
}

function helpMessage(toolbox: GluegunToolbox) {
  toolbox.print.info(``);
  toolbox.print.info(`  Welcome to ${toolbox.print.colors.magenta('atlas-fe')} CLI version ${toolbox.meta.version()}!`);
  toolbox.print.info(`  Refer to ${toolbox.print.colors.cyan('https://bitbucket.org/atlassian/atlas-fe')} to learn how to use the CLI.`);
}

/**
 * Create the cli and kick it off
 */
async function run(argv: string | Options) {
  verifyNodeVersion();

  // create a CLI runtime
  const cli = build()
    .brand('atlas-fe')
    .src(__dirname)
    .defaultCommand(helpMessage)
    .help(helpMessage)
    .version() // provides default for version, v, --version, -v
    .create();

  // run it
  const toolbox = (await cli.run(argv)) as AtlasFeToolbox;

  // track the event in analytics
  await trackExecutedCommand(toolbox);

  // and send it back (for testing, mostly)
  return toolbox;
}

module.exports = { run };
