import { GluegunCommand } from 'gluegun';
import { PackageJSON } from 'gluegun/build/types/toolbox/meta-types';
import type { AtlasFeToolbox } from './extensions/api';

type AtlasFeLifecycles = 'init' | 'build' | 'start' | 'watch' | 'watch-prepare';

export type { AtlasFeToolbox };

export type { PackageJSON };

export interface AtlasFeCommand extends GluegunCommand<AtlasFeToolbox> {
  name: AtlasFeLifecycles | string;
  run: (toolbox: AtlasFeToolbox) => Promise<void> | void;
}

// pom.xml definition - modify as you need.
export interface PomAsJson {
  project: [
    {
      [key: string]: unknown;
      groupId: string;
      artifactId: string;
      properties?: [{ [key: string]: string }];
      build?: [
        {
          plugins?: [
            {
              plugin?: PomPluginAsJson[];
            },
          ];
        },
      ];
      profiles?: [
        {
          profile: { [key: string]: unknown }[];
        },
      ];
    },
  ];
}

export type PomAsJsonOrEmpty =
  | PomAsJson
  | {
      project: '';
    }
  | ''
  | Record<string, never>;

export interface PomPluginAsJson {
  groupId: string;
  artifactId: string;
  version: string;
  configuration?: [
    {
      instructions?: [
        {
          'Atlassian-Plugin-Key'?: string;
          'Export-Package'?: string;
          'Import-Package'?: string;
          'Sprint-Context'?: string;
          'Atlassian-Scan-Folders'?: string;
          [key: string]: unknown;
        },
      ];
    },
  ];
  [key: string]: unknown;
}
