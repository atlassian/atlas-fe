import detectIndent from 'detect-indent';

export interface AtlasFeDetectIndentToolbox {
  /** detects the indentation information of a given string */
  detectIndent: (raw: string) => detectIndent.Indent;
}

export { detectIndent };
