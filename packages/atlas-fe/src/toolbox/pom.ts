import { GluegunToolbox } from 'gluegun';
import { XMLParser, XMLBuilder } from 'fast-xml-parser';

import { detectIndent } from './detect-indent';
import { PomAsJson, PomAsJsonOrEmpty } from '../types';

export interface AtlasFePomToolbox {
  /** pom.xml file operations */
  pom: {
    parseXml: typeof parseXml;
    serializeXml: typeof serializeXml;
    /** gets the content of the pom.xml file in JSON format */
    getPomContent: () => ReturnType<typeof getPomContent>;
    /** writes a PomAsJson object to the pom.xml file */
    writePomContent: (content: PomAsJson, indent: string) => ReturnType<typeof writePomContent>;
    /** reads the pom.xml file and returns the full plugin key [groupdId:artifactId] */
    getPluginKeyFromPom: () => ReturnType<typeof getPluginKeyFromPom>;
  };
}

const POM_PATH = 'pom.xml';

// If the xml is not valid, it will return an empty string
export const parseXml = <T = string | unknown>(xmlContent: string) => {
  const { indent } = detectIndent(xmlContent);

  const parser = new XMLParser({
    ignoreAttributes: false,
  });
  const content = parser.parse(xmlContent) as T;
  const arrayFormContent = objectJsonFormToArrayForm(content);

  return { content: arrayFormContent, indent };
};

function objectJsonFormToArrayForm<T = string | unknown>(obj: T): T {
  if (!isRecord(obj)) {
    return obj;
  }

  const transformedJson: Record<string, unknown> = {};
  for (const propKey of Object.keys(obj)) {
    const propValue = obj[propKey];
    if (isRecord(propValue)) {
      transformedJson[propKey] = [objectJsonFormToArrayForm(propValue)];
    } else if (Array.isArray(propValue)) {
      transformedJson[propKey] = propValue.map(objectJsonFormToArrayForm);
    } else {
      transformedJson[propKey] = propValue;
    }
  }

  return transformedJson as T;
}

function isRecord(something: unknown): something is Record<string, unknown> {
  return typeof something === 'object' && !Array.isArray(something) && something !== null;
}

export const serializeXml = (content: unknown, indent: string) => {
  const builder = new XMLBuilder({
    indentBy: indent,
    format: true,
    ignoreAttributes: false,
  });

  return builder.build(content);
};

export const getPomContent = async (toolbox: GluegunToolbox) => {
  if (!toolbox.filesystem.isFile(POM_PATH)) {
    throw new Error('atlas-fe: pom.xml file not found. Aborting...');
  }

  const rawPomContent = await toolbox.filesystem.readAsync(POM_PATH).catch(() => {
    throw new Error('atlas-fe: is not possible to read the pom.xml file. Aborting...');
  });

  if (!rawPomContent) {
    throw new Error('atlas-fe: pom.xml file is empty. Aborting...');
  }

  const { content, indent } = parseXml<PomAsJsonOrEmpty>(rawPomContent);

  if (content === '' || Object.keys(content).length === 0 || content.project === '') {
    throw new Error("atlas-fe: pom.xml file could't be parsed, possibly invalid file. Aborting...");
  }

  return { content, indent };
};

export const writePomContent = async (toolbox: GluegunToolbox, content: PomAsJson, indent = '  ') => {
  try {
    const serializedContent = serializeXml(content, indent);

    await toolbox.filesystem.writeAsync(POM_PATH, serializedContent);
  } catch (error) {
    throw new Error("atlas-fe: can't write pom.xml file. Aborting...");
  }
};

export const getPluginKeyFromPom = async (toolbox: GluegunToolbox) => {
  const { content } = await getPomContent(toolbox);
  const { artifactId, groupId } = content.project[0];

  if (!artifactId || !groupId) {
    throw new Error(`atlas-fe: pom.xml file does not contain a group or artifact id.`);
  }

  const pluginKey = `${groupId}.${artifactId}`;

  return pluginKey;
};
