/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { mocked } from 'jest-mock';

import { gluegunToolboxMock, ToolboxMock } from '../testing/toolbox-mock';
import { AtlasFeToolbox } from '../types';
import { AtlasFeConfig } from './config';
import { exists, run, loadPlugins, loadPluginsFromConfig } from './plugins';

/**
 * We need to create these testing interfaces because the real Gluegun interaces are really complex,
 * especially the runtime, and we are just concerned about the list of plugins and plugin metadata,
 * not mocking the whole runtime.
 */
interface TestingCommand {
  name: string;
  run: (toolbox: AtlasFeToolbox) => Promise<void> | void;
  plugin?: TestingPlugin;
}

interface TestingPlugin {
  name: string;
  commands: TestingCommand[];
}

interface TestingRuntime {
  plugins?: TestingPlugin[];
  addPlugin?: (name: string) => void;
}

const getCommand = (testingPlugin: TestingPlugin, name: string) => testingPlugin.commands.find((command) => command.name === name);

describe('plugins toolbox', () => {
  let testingToolbox: ToolboxMock;
  let testingCliPlugin: TestingPlugin;
  let testingPlugin_1: TestingPlugin;
  let testingPlugin_2: TestingPlugin;

  beforeEach(() => {
    testingToolbox = { ...gluegunToolboxMock };
    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [],
      addPlugin: jest.fn(),
    };

    // The runtime holds all the plugin definitions when it starts.
    // The first plugin is always the CLI itself
    testingCliPlugin = {
      name: 'fake-cli',
      commands: [
        {
          name: 'init',
          run: jest.fn(),
          plugin: testingCliPlugin,
        },
        {
          name: 'start',
          run: jest.fn(),
          plugin: testingCliPlugin,
        },
      ],
    };
    testingPlugin_1 = {
      name: 'testing-plugin-1',
      commands: [
        {
          name: 'init',
          run: jest.fn(),
          plugin: testingPlugin_1,
        },
        {
          name: 'start',
          run: jest.fn(),
          plugin: testingPlugin_1,
        },
      ],
    };
    testingPlugin_2 = {
      name: 'testing-plugin-2',
      commands: [
        {
          name: 'init',
          run: jest.fn(),
          plugin: testingPlugin_2,
        },
      ],
    };
  });

  it('should return if a plugin exists in the runtime', () => {
    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [testingPlugin_1],
    };

    expect(exists(testingToolbox, 'testing-plugin-1')).toBe(true);
    expect(exists(testingToolbox, 'not-existing-plugin')).toBe(false);

    // empty plugins
    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [],
    };
    expect(exists(testingToolbox, 'testing-plugin-1')).toBe(false);

    // broken runtime
    (testingToolbox.runtime as TestingRuntime) = {};
    expect(exists(testingToolbox, 'testing-plugin-1')).toBe(false);

    // no runtime
    testingToolbox.runtime = undefined;
    expect(exists(testingToolbox, 'testing-plugin-1')).toBe(false);
  });

  it('should not run any command of the CLI plugin (first plugin on the list)', async () => {
    expect(getCommand(testingCliPlugin, 'init')?.run).toBeCalledTimes(0);
    expect(getCommand(testingCliPlugin, 'start')?.run).toBeCalledTimes(0);

    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [testingCliPlugin],
    };

    await run(testingToolbox, 'init');
    await run(testingToolbox, 'start');

    expect(getCommand(testingCliPlugin, 'init')?.run).toBeCalledTimes(0);
    expect(getCommand(testingCliPlugin, 'start')?.run).toBeCalledTimes(0);
  });

  it('should run a command in all the plugins that have an implementation for it', async () => {
    expect(getCommand(testingPlugin_1, 'init')?.run).toBeCalledTimes(0);
    expect(getCommand(testingPlugin_1, 'start')?.run).toBeCalledTimes(0);
    expect(getCommand(testingPlugin_2, 'init')?.run).toBeCalledTimes(0);

    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [testingCliPlugin, testingPlugin_1, testingPlugin_2],
    };

    await run(testingToolbox, 'init');

    expect(getCommand(testingPlugin_1, 'init')?.run).toBeCalledTimes(1);
    expect(getCommand(testingPlugin_1, 'start')?.run).toBeCalledTimes(0);
    expect(getCommand(testingPlugin_2, 'init')?.run).toBeCalledTimes(1);

    await run(testingToolbox, 'start');

    expect(getCommand(testingPlugin_1, 'init')?.run).toBeCalledTimes(1);
    expect(getCommand(testingPlugin_1, 'start')?.run).toBeCalledTimes(1);
    expect(getCommand(testingPlugin_2, 'init')?.run).toBeCalledTimes(1);
  });

  it('should accept sync and async commands and execute them in the right order', async () => {
    const executionOrder: string[] = [];
    mocked(getCommand(testingPlugin_1, 'init')!.run).mockImplementation(() => {
      executionOrder.push(testingPlugin_1.name);
      return Promise.resolve();
    });
    mocked(getCommand(testingPlugin_2, 'init')!.run).mockImplementation(() => {
      executionOrder.push(testingPlugin_2.name);
      return;
    });
    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [testingCliPlugin, testingPlugin_1, testingPlugin_2],
    };

    expect(getCommand(testingPlugin_1, 'init')?.run).toBeCalledTimes(0);
    expect(getCommand(testingPlugin_2, 'init')?.run).toBeCalledTimes(0);

    await run(testingToolbox, 'init');

    expect(getCommand(testingPlugin_1, 'init')?.run).toBeCalledTimes(1);
    expect(getCommand(testingPlugin_2, 'init')?.run).toBeCalledTimes(1);
    expect(executionOrder).toEqual([testingPlugin_1.name, testingPlugin_2.name]);
  });

  it('should print a message with the plugin name and the command executed when the CLI is running on development mode', async () => {
    (testingToolbox.runtime as TestingRuntime) = {
      plugins: [testingCliPlugin, testingPlugin_1, testingPlugin_2],
    };

    expect(mocked(testingToolbox.print.divider)).toBeCalledTimes(0);
    expect(mocked(testingToolbox.print.info)).toBeCalledTimes(0);

    // production mode
    testingToolbox.config.isDevelopment = false;

    await run(testingToolbox, 'init');

    expect(mocked(testingToolbox.print.divider)).toBeCalledTimes(0);
    expect(mocked(testingToolbox.print.info)).toBeCalledTimes(0);

    // development mode
    testingToolbox.config.isDevelopment = true;

    await run(testingToolbox, 'init');

    expect(mocked(testingToolbox.print.divider)).toBeCalledTimes(4);
    expect(mocked(testingToolbox.print.info)).toHaveBeenNthCalledWith(1, `Running ${testingPlugin_1.name}:init `);
    expect(mocked(testingToolbox.print.info)).toHaveBeenNthCalledWith(2, `Running ${testingPlugin_2.name}:init `);
  });

  it('should load a given list of plugins from node_modules', () => {
    const testingPluginList = ['@atlassian/atlas-fe-testing', 'atlas-fe-custom-plugin'];

    loadPlugins(testingToolbox, testingPluginList);

    testingPluginList.forEach((pluginName) =>
      expect(mocked(testingToolbox.runtime?.addPlugin)).toHaveBeenCalledWith(`./node_modules/${pluginName}`),
    );
  });

  it('should load the plugins specified in atlas-fe configuration file', () => {
    const testingConfig: AtlasFeConfig = {
      entries: {},
      pluginKey: 'fake-plugin-key',
      isDevelopment: false,
      loadConfig: () => testingConfig,
      output: './not-in-use', // not really in use, but requirerd to be part of the AtlasFeConfig type
      plugins: [
        '@atlassian/atlas-fe-string-plugin',
        {
          plugin: '@atlassian/atlas-fe-object-plugin',
          config: {}, // this configuration object can be use by plugin devs to provide some customization features
        },
      ],
    };
    const expectedPluginsLoaded = ['@atlassian/atlas-fe-string-plugin', '@atlassian/atlas-fe-object-plugin'];

    loadPluginsFromConfig(testingToolbox, testingConfig);

    expectedPluginsLoaded.forEach((pluginName) =>
      expect(mocked(testingToolbox.runtime?.addPlugin)).toHaveBeenCalledWith(`./node_modules/${pluginName}`),
    );
  });

  it('should not load any plugins if none is specified in atlas-fe.config.js configuration file', () => {
    const testingConfig: AtlasFeConfig = {
      entries: {},
      pluginKey: 'fake-plugin-key',
      isDevelopment: false,
      loadConfig: () => testingConfig,
      output: './not-in-use', // not really in use, but requirerd to be part of the AtlasFeConfig type
      // no `plugins` key
    };

    loadPluginsFromConfig(testingToolbox, testingConfig);

    expect(mocked(testingToolbox.runtime?.addPlugin)).not.toHaveBeenCalled();
  });

  it('should throw if the configuration is invalid when loading plugins from atlas-fe.config.js', () => {
    let testingConfig: AtlasFeConfig = {
      entries: {},
      pluginKey: 'fake-plugin-key',
      isDevelopment: false,
      loadConfig: () => testingConfig,
      output: './not-in-use', // not really in use, but requirerd to be part of the AtlasFeConfig type
    };

    // invalid plugin configuration
    testingConfig = {
      ...testingConfig,
      // @ts-expect-error the value is not a valid plugin configuration
      plugins: `invalid-value`,
    };

    expect(() => loadPluginsFromConfig(testingToolbox, testingConfig)).toThrowError(
      'Invalid plugin configuration found in atlas-fe.config.js',
    );

    // invalid plugin declaration
    testingConfig = {
      ...testingConfig,
      plugins: [
        {
          // @ts-expect-error typo in `plugin` property. This should cause an error when evaluating the plugin
          plgin: '@atlassian/atlas-fe-testing',
          config: {},
        },
      ],
    };

    expect(() => loadPluginsFromConfig(testingToolbox, testingConfig)).toThrowError(
      'Invalid plugin declaration found in atlas-fe.config.js:',
    );
  });
});
