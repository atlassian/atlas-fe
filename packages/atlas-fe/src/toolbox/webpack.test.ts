import webpack from 'webpack';

import {
  webpackConfig as INTERNAL_WEBPACK_CONFIG,
  chainWebpackConfig,
  resolveWebpackConfig,
  addWrmProvidedDependencies,
  AtlasFeWebpackConfig,
} from './webpack';

class FakeWrmPlugin {
  constructor(private options: Record<string, unknown>) {}

  apply(compiler: unknown) {
    return compiler;
  }
}

describe('webpack toolbox', () => {
  const configureWebpack = (config: AtlasFeWebpackConfig) => {
    config.entry('fake-js').add('./fake-path.js');
    config.entry('fake-ts').add('./fake-path.ts');
    config.output.path('/src/fake-dist/').filename('[name].bundle.js');
    config.mode('development');
  };

  beforeEach(() => {
    INTERNAL_WEBPACK_CONFIG.entryPoints.clear();
    INTERNAL_WEBPACK_CONFIG.output.clear();
    INTERNAL_WEBPACK_CONFIG.mode('none');
    INTERNAL_WEBPACK_CONFIG.plugins.clear();
  });

  it('should be able to modify the internal webpack configuration', () => {
    chainWebpackConfig(configureWebpack);

    expect(INTERNAL_WEBPACK_CONFIG.entryPoints.entries()).toMatchObject({
      'fake-js': {
        store: new Set(['./fake-path.js']),
      },
      'fake-ts': {
        store: new Set(['./fake-path.ts']),
      },
    });
    expect(INTERNAL_WEBPACK_CONFIG.output.entries()).toMatchObject({
      path: '/src/fake-dist/',
      filename: '[name].bundle.js',
    });
    expect(INTERNAL_WEBPACK_CONFIG.get('mode')).toBe('development');
  });

  it('should resolve into a valid webpack configuration', () => {
    // Add some testing configuration
    chainWebpackConfig(configureWebpack);

    const config = resolveWebpackConfig();

    expect(config).toEqual({
      output: { path: '/src/fake-dist/', filename: '[name].bundle.js' },
      entry: { 'fake-js': ['./fake-path.js'], 'fake-ts': ['./fake-path.ts'] },
      mode: 'development',
    });
    expect(webpack(config)).toBeTruthy();
  });

  it('should allow adding provided dependencies to the WRM webpack plugin', () => {
    const testingProvidedDependencies = {
      'dependency-name': {
        dependency: 'atlassian.plugin.key:webresource-key',
        import: {
          var: "require('dependency-amd-module-name')",
          amd: 'dependency-amd-module-name',
        },
      },
    };
    chainWebpackConfig((config) => {
      config.plugin('WrmPlugin').use(FakeWrmPlugin, [
        {
          pluginKey: 'fake-plugin',
        },
      ]);
    });

    addWrmProvidedDependencies(testingProvidedDependencies);

    expect(INTERNAL_WEBPACK_CONFIG.plugin('WrmPlugin').entries().args).toEqual([
      {
        pluginKey: 'fake-plugin',
        providedDependencies: testingProvidedDependencies,
      },
    ]);
  });
});
