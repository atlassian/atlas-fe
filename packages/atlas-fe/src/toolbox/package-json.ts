import { GluegunToolbox } from 'gluegun';
import { PackageJSON } from '../types';
import { detectIndent } from './detect-indent';

export type WritePackageJsonCallback = (file: PackageJSON) => PackageJSON;

export interface AtlasFePackageJson {
  /** package.json file operations */
  packageJson: {
    /** returns the content of package.json file in JSON format */
    read: () => Promise<{ content: PackageJSON; indent: number }>;
    /** writes the gien content to the package.json file of the project, and returns the final result */
    write: (cb: WritePackageJsonCallback) => Promise<PackageJSON>;
  };
}

export const readPackageJson = async (toolbox: GluegunToolbox) => {
  const rawPackageJson = await toolbox.filesystem.readAsync('package.json');

  if (!rawPackageJson) {
    throw new Error('package.json file not found.');
  }

  const { amount: indent } = detectIndent(rawPackageJson);
  const content = JSON.parse(rawPackageJson) as PackageJSON;

  return { content, indent };
};

export const writePackageJson = async (cb: WritePackageJsonCallback, toolbox: GluegunToolbox) => {
  const { content: packageJsonContent, indent: jsonIndent } = await readPackageJson(toolbox);

  const modifiedPackageJsonContent = cb(packageJsonContent);

  await toolbox.filesystem.writeAsync('package.json', modifiedPackageJsonContent, { jsonIndent });

  return modifiedPackageJsonContent;
};
