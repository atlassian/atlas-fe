import { GluegunToolbox } from 'gluegun';

import { AtlasFeCommand, AtlasFeToolbox } from '../types';
import { AtlasFeConfig } from './config';

export interface AtlasFePluginsToolbox {
  /** plugins operations */
  plugins: {
    /** Checks if a plugin exists */
    exists: (name: string) => boolean;
    /** Executes a given command on each plugin that has it */
    run: (name: string) => Promise<void>;
    /** Load a lit of plugins from node_modules using their package name */
    load: (packages: string[]) => void;
  };
}

export const exists = (toolbox: GluegunToolbox, name: string) => {
  return !!toolbox.runtime?.plugins?.find((plugin) => plugin.name === name);
};

export const run = async (toolbox: GluegunToolbox, commandToExecute: string) => {
  // get all the plugins, the first one  always beign the altas-fe CLI.
  const [cli, ...plugins] = toolbox.runtime?.plugins || [];

  // get a list of all the plugins that have an implementation of the given commmand
  const commands = plugins.reduce((commands, plugin) => {
    const commandInPlugin = plugin.commands.find((command) => command.name === commandToExecute);

    return commandInPlugin ? [...commands, commandInPlugin] : commands;
  }, [] as AtlasFeCommand[]);

  // execute init command of all our plugins
  for (const command of commands) {
    if ((toolbox.config as AtlasFeToolbox).isDevelopment) {
      toolbox.print.divider();
      toolbox.print.info(`Running ${command.plugin?.name}:${commandToExecute} `);
      toolbox.print.divider();
    }

    // set toolbox plugin ref to the plugin of the command being executed
    toolbox.plugin = command.plugin;

    try {
      await command.run(toolbox as AtlasFeToolbox);
    } catch (error) {
      // print a nicer error message
      toolbox.print.error((error as Error).message);
      // and kill the process with error signal
      process.exit(1);
    }
  }

  // set toolbox plugin ref back to CLI
  toolbox.plugin = cli;
};

export const loadPlugins = (toolbox: GluegunToolbox, packages: string[]) => {
  packages.forEach((pluginName) => {
    // load the plugin from node_modules
    // TODO: would it make sense to allow developers to load "local" plugins like Vue CLI does?
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion -- should fail if it can't load a plugin
    toolbox.runtime!.addPlugin(`./node_modules/${pluginName}`);
  });
};

export const loadPluginsFromConfig = (toolbox: GluegunToolbox, config: AtlasFeConfig) => {
  // no plugins to load. skip.
  if (!config.plugins) {
    return;
  }

  if (!toolbox.runtime) {
    throw new Error('Runtime is not present. Execute your command using "atlas-fe <command>"');
  }

  if (!Array.isArray(config.plugins)) {
    throw new Error('Invalid plugin configuration found in atlas-fe.config.js');
  }

  const pluginsToLoad = config.plugins.map((pluginDeclaration) => {
    // the plugin declaration can either be a string with the name of the plugin, or an object with the name of the plugin
    const pluginName = typeof pluginDeclaration === 'string' ? pluginDeclaration : pluginDeclaration.plugin || '';

    // stop the execution of the command if there's something wrong with a plugin declaration
    if (pluginName === '') {
      throw new Error(`Invalid plugin declaration found in atlas-fe.config.js: ${pluginDeclaration}`);
    }

    return pluginName;
  });

  loadPlugins(toolbox, pluginsToLoad);

  config.isDevelopment && toolbox.print.debug(pluginsToLoad, 'Plugins loaded');
};
