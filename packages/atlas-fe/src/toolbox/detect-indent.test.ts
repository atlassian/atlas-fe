import outdent from 'outdent';
import { detectIndent } from './detect-indent';

describe('detectIndent toolbox', () => {
  it('should detect the indentation used on a string', () => {
    const testString = outdent`
      const name = "fake-variable";

      function print(otherName) {
        if (otherName) {
          console.log(otherName);
          return;
        }

        console.log(name);
      }
    `;

    const { amount, indent, type } = detectIndent(testString);

    expect(amount).toBe(2);
    expect(indent).toBe('  ');
    expect(type).toBe('space');
  });
});
