import Config = require('webpack-chain');

/** singleton webpack configuration object */
export const webpackConfig = new Config();

export interface AtlasFeWebpackToolbox {
  /** webpack operations */
  webpack: {
    /** provides the global webpack configuration object in order to modify it before passing it to webpack */
    chainWebpackConfig: typeof chainWebpackConfig;
    /** returns the final webpack configuration */
    resolveWebpackConfig: typeof resolveWebpackConfig;
    /** allows to add provided dependencies declarations to use by WRM webpack plugin */
    addWrmProvidedDependencies: typeof addWrmProvidedDependencies;
  };
}

export type AtlasFeWebpackConfig = Config;

export const chainWebpackConfig = (cb: (config: AtlasFeWebpackConfig) => void) => cb(webpackConfig);

export const resolveWebpackConfig = () => webpackConfig.toConfig();

export const addWrmProvidedDependencies = (dependencies: { [name: string]: unknown }) => {
  webpackConfig.plugin('WrmPlugin').tap(([config]) => {
    return [
      {
        ...config,
        providedDependencies: { ...config.providedDependencies, ...dependencies },
      },
    ];
  });
};
