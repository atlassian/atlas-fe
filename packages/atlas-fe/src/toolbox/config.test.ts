import { mocked } from 'jest-mock';

import { gluegunToolboxMock, ToolboxMock } from '../testing/toolbox-mock';
import { getConfig } from './config';

describe('config toolbox', () => {
  it('should detect that the CLI is being executed in development mode in test environment', () => {
    const config = getConfig({ ...gluegunToolboxMock });

    expect(mocked(gluegunToolboxMock.print.warning)).toHaveBeenNthCalledWith(
      1,
      'Development mode detected, local packages will be used instead of production.',
    );
    expect(mocked(gluegunToolboxMock.print.warning)).toHaveBeenNthCalledWith(
      2,
      `If this is not what you want, run 'yarn packages:unlink' from the atlas-fe repository and install @atlassian/atlas-fe globally instead.`,
    );
    expect(config.isDevelopment).toBe(true);
  });

  it('should not print the "on development" message when executing in production mode', () => {
    const testingToolbox: ToolboxMock = {
      ...gluegunToolboxMock,
      filesystem: {
        ...gluegunToolboxMock.filesystem,
        // should tell config that we are running as an installed packaged rather than linked packaged (development)
        exists: jest.fn(() => false),
      },
    };

    const config = getConfig(testingToolbox);

    expect(mocked(gluegunToolboxMock.print.warning)).not.toHaveBeenCalled();
    expect(config.isDevelopment).toBe(false);
  });

  it('should set a default output path configuration if none is provided', () => {
    const testingToolbox: ToolboxMock = {
      ...gluegunToolboxMock,
      filesystem: {
        ...gluegunToolboxMock.filesystem,
        path: (...args) => args.join('/'),
      },
    };

    const config = getConfig(testingToolbox);

    expect(config.output).toBe('target/classes');
  });

  it('should load the user configuration from atlas-fe.config.js', () => {
    const testingConfig = {
      entries: {
        'atl.general': 'a/fake/path',
      },
      pluginKey: 'fake-plugin-key',
      output: 'fake/path', // this should be preserved
      isDevelopment: false, // this should be ignored and set by getConfig instead
    };
    const testingToolbox = {
      ...gluegunToolboxMock,
      config: {
        loadConfig: jest.fn(() => testingConfig),
      },
    } as ToolboxMock;

    const config = getConfig(testingToolbox);

    expect(config.entries).toMatchObject(testingConfig.entries);
    expect(config.pluginKey).toBe(testingConfig.pluginKey);
    expect(config.output).toBe(testingConfig.output);
    expect(config.isDevelopment).toBe(true); // developmet is true when running extension from source rather than build
    // Gluegun toolbox expect to receive the file name and the folder where to look for it
    expect(mocked(testingToolbox.config.loadConfig)).toHaveBeenCalledWith('atlas-fe', process.cwd());
  });
});
