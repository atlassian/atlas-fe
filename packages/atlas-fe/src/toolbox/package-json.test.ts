import { mocked } from 'jest-mock';

import { PackageJSON } from '../types';
import { TestingReadAsync, TestingWriteAsync, gluegunToolboxMock, ToolboxMock } from '../testing/toolbox-mock';
import { readPackageJson, writePackageJson } from './package-json';

describe('package-json toolbox', () => {
  let testingPackageJson: PackageJSON;
  let testingToolbox: ToolboxMock;
  const testingIndent = 2;

  beforeEach(() => {
    // clear the testing package.json
    testingPackageJson = {
      version: '0.0.0',
      name: 'fake-package',
      license: 'ISC',
      scripts: {
        test: 'echo "Error: no test specified" && exit 1',
      },
    };

    // recreate the testing toolbox
    testingToolbox = {
      ...gluegunToolboxMock,
      filesystem: {
        ...gluegunToolboxMock.filesystem,
      },
    };
    // Trick to replace a method that has multiple signatures (ovreloaded) and make TS happy
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve(JSON.stringify(testingPackageJson, null, testingIndent)),
    );
    (testingToolbox.filesystem.writeAsync as TestingWriteAsync) = jest.fn((path, content) => {
      testingPackageJson = content;
    });
  });

  it('should throw an error if the file is not found or the content is empty', async () => {
    // replace readAsync method to return an empty content
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve(undefined));

    await expect(readPackageJson(testingToolbox)).rejects.toThrow('package.json file not found.');
  });

  it('should return the content and indent of the package.json file', async () => {
    const { content, indent } = await readPackageJson(testingToolbox);

    expect(content).toMatchObject(testingPackageJson);
    expect(indent).toBe(testingIndent);
  });

  it('should write new content to the package.json file', async () => {
    const modifiedContent = await writePackageJson((packageJson) => {
      // we should receive the testing package.json file
      expect(packageJson).toMatchObject(testingPackageJson);

      const newContent: PackageJSON = {
        ...packageJson,
        homepage: 'a-fake-homepage',
        keywords: ['testing', 'keywords'],
        main: 'dist/index.js',
      };

      return newContent;
    }, testingToolbox);

    expect(testingPackageJson).toMatchObject(modifiedContent);
    // indent should be kept the same as the indent from the package.json file
    expect(mocked(testingToolbox.filesystem.writeAsync)).toHaveBeenCalledWith('package.json', expect.anything(), {
      jsonIndent: testingIndent,
    });
  });
});
