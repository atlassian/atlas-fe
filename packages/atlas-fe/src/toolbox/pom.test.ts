import { mocked } from 'jest-mock';
import outdent from 'outdent';

import { TestingReadAsync, gluegunToolboxMock, ToolboxMock } from '../testing/toolbox-mock';
import { getPomContent, writePomContent, getPluginKeyFromPom } from './pom';
import { PomAsJson } from '../types';

type TestingWriteAsync = (path: string, content: string) => void;

const POM_CONTENT_MOCK = outdent`
  <project xmlns="http://maven.apache.org/POM/4.0.0">
    <groupId>com.atlassian.atlas-fe</groupId>
    <artifactId>atlas-fe-testing</artifactId>
    <properties>
      <refapp.version>5.0.0</refapp.version>
    </properties>
    <build>
      <plugins>
        <plugin>
          <groupId>com.atlassian.fake</groupId>
          <artifactId>fake-plugin</artifactId>
          <version>1.0.0</version>
        </plugin>
      </plugins>
    </build>
  </project>
`;

describe('pom toolbox', () => {
  let testingToolbox: ToolboxMock;
  let testingPomContent: string;

  beforeEach(() => {
    testingPomContent = POM_CONTENT_MOCK;

    testingToolbox = {
      ...gluegunToolboxMock,
      filesystem: {
        ...gluegunToolboxMock.filesystem,
        isFile: jest.fn(() => true),
      },
    };

    // Trick to replace a method that has multiple signatures (ovreloaded) and make TS happy
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve(testingPomContent));
    (testingToolbox.filesystem.writeAsync as TestingWriteAsync) = jest.fn((path, content) => {
      testingPomContent = content as string;
    });
  });

  it('should return the content of pom.xml file as a json object and its indent value', async () => {
    const { content, indent } = await getPomContent(testingToolbox);

    // should load the root pom.xml file
    expect(mocked(testingToolbox.filesystem.isFile)).toHaveBeenCalledWith('pom.xml');
    expect(indent).toBe('  ');
    // If the critical properties are there, is will still work.
    expect(content).toMatchObject({
      project: [
        {
          groupId: 'com.atlassian.atlas-fe',
          artifactId: 'atlas-fe-testing',
          '@_xmlns': 'http://maven.apache.org/POM/4.0.0',
          properties: [
            {
              'refapp.version': '5.0.0',
            },
          ],
          build: [
            {
              plugins: [
                {
                  plugin: [
                    {
                      artifactId: 'fake-plugin',
                      groupId: 'com.atlassian.fake',
                      version: '1.0.0',
                    },
                  ],
                },
              ],
            },
          ],
        },
      ],
    });
  });

  it("should throw an error if the pom.xml file doesn't exists", async () => {
    mocked(testingToolbox.filesystem.isFile).mockImplementation(() => false);

    await expect(getPomContent(testingToolbox)).rejects.toThrow('atlas-fe: pom.xml file not found. Aborting...');
  });

  it("should throw an error if the pom.xml can't be read", async () => {
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.reject());

    await expect(getPomContent(testingToolbox)).rejects.toThrow('atlas-fe: is not possible to read the pom.xml file. Aborting...');
  });

  it("should throw an error if the pom.xml file doesn't have any content", async () => {
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve(''));

    await expect(getPomContent(testingToolbox)).rejects.toThrow('atlas-fe: pom.xml file is empty. Aborting...');
  });

  it("should throw an error if the content of the pom.xml file can't be converted to json", async () => {
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve('invalid XML file @@@@@'));

    await expect(getPomContent(testingToolbox)).rejects.toThrow(
      "atlas-fe: pom.xml file could't be parsed, possibly invalid file. Aborting...",
    );

    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() => Promise.resolve('<project></project>'));

    await expect(getPomContent(testingToolbox)).rejects.toThrow(
      "atlas-fe: pom.xml file could't be parsed, possibly invalid file. Aborting...",
    );
  });

  it('should get the plugin key from the pom.xml file', async () => {
    const pluginKey = await getPluginKeyFromPom(testingToolbox);

    expect(pluginKey).toBe('com.atlassian.atlas-fe.atlas-fe-testing');
  });

  it("should throw an error if the plugin key can't be read from the pom.xml file", async () => {
    // missing artifactId and groupIdproperties
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve('<project><properties></properties></project>'),
    );

    await expect(getPluginKeyFromPom(testingToolbox)).rejects.toThrow('atlas-fe: pom.xml file does not contain a group or artifact id.');

    // missing artifactId
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve('<project><groupId>com.atlassian.atlas-fe</groupId></project>'),
    );

    await expect(getPluginKeyFromPom(testingToolbox)).rejects.toThrow('atlas-fe: pom.xml file does not contain a group or artifact id.');

    // missing groupId
    (testingToolbox.filesystem.readAsync as TestingReadAsync) = jest.fn(() =>
      Promise.resolve('<project><artifactId>atlas-fe-testing</artifactId></project>'),
    );

    await expect(getPluginKeyFromPom(testingToolbox)).rejects.toThrow('atlas-fe: pom.xml file does not contain a group or artifact id.');
  });

  it('should write a new content to the pom.xml file', async () => {
    const newPomContent: PomAsJson = {
      project: [
        {
          groupId: 'com.atlassian.atlas-fe',
          artifactId: 'atlas-fe-testing',
          properties: [
            {
              'refapp.version': '5.0.0',
            },
          ],
          '@_xmlns': 'http://maven.apache.org/POM/4.0.0',
        },
      ],
    };
    // an empty line is added to the end of the file when written. Don't remove.
    const newPomContentExpected = outdent`
      <project xmlns="http://maven.apache.org/POM/4.0.0">
        <groupId>com.atlassian.atlas-fe</groupId>
        <artifactId>atlas-fe-testing</artifactId>
        <properties>
          <refapp.version>5.0.0</refapp.version>
        </properties>
      </project>

    `;

    writePomContent(testingToolbox, newPomContent);

    expect(testingPomContent).toBe(newPomContentExpected);
  });

  it("should throw an error if the pom.xml can't be written", async () => {
    const newPomContent: PomAsJson = {
      project: [
        {
          groupId: 'com.atlassian.atlas-fe',
          artifactId: 'atlas-fe-testing',
        },
      ],
    };
    (testingToolbox.filesystem.writeAsync as TestingReadAsync) = jest.fn(() => Promise.reject());

    await expect(writePomContent(testingToolbox, newPomContent)).rejects.toThrow("atlas-fe: can't write pom.xml file. Aborting...");
  });
});
