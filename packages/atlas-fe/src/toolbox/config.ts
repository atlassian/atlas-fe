import { GluegunToolbox } from 'gluegun';
import type { AtlasFeWebpackConfig } from './webpack';

export type AtlasFePluginDeclaration =
  | string
  | {
      plugin: string;
      config: unknown;
    };

export interface AtlasFeConfig {
  pluginKey: string;
  entries: Record<string, string>;
  output: string;
  isDevelopment: boolean;
  i18nFiles?: string[];
  providedDependencies?: {
    [key: string]: unknown;
  };
  chainWebpackConfig?: (webpackConfig: AtlasFeWebpackConfig, command: string) => void;
  plugins?: AtlasFePluginDeclaration[];
  loadConfig: (name: string, src: string) => AtlasFeConfig; // comes from Gluegun Toolbox
  [key: string]: unknown;
}

export interface AtlasFeConfigToolbox {
  /** developer configuration */
  config: AtlasFeConfig;
}

export const getConfig = (toolbox: GluegunToolbox): AtlasFeConfig => {
  // if we're running from src instead of build, we're on development mode
  const isDeveloment = !!toolbox.filesystem.exists(`${__dirname}/../../src`);

  if (isDeveloment) {
    toolbox.print.warning(`Development mode detected, local packages will be used instead of production.`);
    toolbox.print.warning(
      `If this is not what you want, run 'yarn packages:unlink' from the atlas-fe repository and install @atlassian/atlas-fe globally instead.`,
    );
    toolbox.print.newline();
  }

  const defaultConfig = {
    output: toolbox.filesystem.path('target', 'classes'),
  };

  // read configuration from the current folder's package.json (in an "atlas-fe" property),
  // atlas-fe.config.json, etc.
  const userConfig = toolbox.config.loadConfig('atlas-fe', process.cwd()) as AtlasFeConfig;

  return {
    ...toolbox.config, // there is something in config already: Gluegun config tools (loadConfig)
    ...defaultConfig,
    ...userConfig,
    isDevelopment: isDeveloment,
  };
};
