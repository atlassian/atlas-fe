import { outdent } from 'outdent';

import { render } from './template';

describe('template toolbox', () => {
  it('should render and return a given EJS template string', () => {
    const testingTemplate = outdent`
      const name = "<%= props.name %>";
      const age = <%= props.age %>;
    `;
    const expectedTemplate = outdent`
      const name = "Jon Doe";
      const age = 24;
    `;

    const result = render(testingTemplate, { name: 'Jon Doe', age: 24 });

    expect(result).toBe(expectedTemplate);
  });

  it('should have Gluegun strings utilities available when rendering a template', () => {
    const testingTemplate = outdent`
      const singular = "<%= props.word %>";
      const plural = "<%= plural(props.word) %>";
      const lowerCase = "<%= lowerCase(props.phrase) %>";
      const upperCase = "<%= upperCase(props.phrase) %>";
      const snakeCase = "<%= snakeCase(props.phrase) %>";
    `;
    const expectedTemplate = outdent`
      const singular = "apple";
      const plural = "apples";
      const lowerCase = "green apples";
      const upperCase = "GREEN APPLES";
      const snakeCase = "green_apples";
    `;

    const result = render(testingTemplate, { word: 'apple', phrase: 'Green Apples' });

    expect(result).toBe(expectedTemplate);
  });
});
