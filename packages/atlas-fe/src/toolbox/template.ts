import ejs from 'ejs';
import { GluegunTemplate, strings } from 'gluegun';

interface AtlasFeTemplate {
  /** takes an EJS string, renders the content with the props given and return the resulted content as a string */
  render: typeof render;
}

export interface AtlasFeTemplateToolbox {
  /** template operations */
  template?: GluegunTemplate & AtlasFeTemplate;
}

interface Props {
  [key: string]: unknown;
}

export const render = (template: string, props: Props = {}) => {
  const data = {
    props,
    ...strings, // gluegun string utilities
  };

  const content = ejs.render(template, data);

  return content;
};
