declare module 'atlassian-webresource-webpack-plugin' {
  class IWrmPlugin {
    public constructor(options: Record<string, unknown>);

    apply(compiler: unknown): void;
  }

  export default IWrmPlugin;
}

declare module 'npm-run-all' {
  interface Options {
    parallel?: boolean;
    stdin?: typeof process.stdin;
    stdout?: typeof process.stdout;
    stderr?: typeof process.stderr;
  }

  export default function (patterns: string | string[], options?: Options): Promise<void>;
}

declare module 'webpack-format-messages' {
  import webpack from 'webpack';

  export default function (stats: webpack.Stats): {
    errors: unknown[];
    warnings: unknown[];
  };
}
