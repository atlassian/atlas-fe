import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock, chainWrmPluginMock } from '@atlassian/atlas-fe/testing';

import command from './watch-prepare';

describe('watch-prepare command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    chainWrmPluginMock(testingToolbox);
  });

  it(`should be named "watch-prepare"`, () => {
    expect(command.name).toBe('watch-prepare');
  });

  it(`should configure the CSE webpack plugin`, async () => {
    await command.run(testingToolbox);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // If the plugin is there, is safe to asome it all went well
      expect(webpackConfig.plugins.has('CSE')).toBe(true);
    });
  });
});
