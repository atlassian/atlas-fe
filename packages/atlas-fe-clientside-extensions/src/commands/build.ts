import { AtlasFeCommand } from '@atlassian/atlas-fe';
import chainCsePlugin from '../utils/chain-cse-plugin';

const COMMAND_NAME = 'build';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    chainCsePlugin(toolbox);
  },
};

export default command;
