import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock, chainWrmPluginMock } from '@atlassian/atlas-fe/testing';

import command from './build';

describe('build command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    chainWrmPluginMock(testingToolbox);
  });

  it(`should be named "build"`, () => {
    expect(command.name).toBe('build');
  });

  it(`should configure the CSE webpack plugin`, async () => {
    await command.run(testingToolbox);

    // it's easier to assert the webpack object than the config when looking for plugins
    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // If the plugin is there, is safe to asome it all went well
      expect(webpackConfig.plugins.has('CSE')).toBe(true);
    });
  });
});
