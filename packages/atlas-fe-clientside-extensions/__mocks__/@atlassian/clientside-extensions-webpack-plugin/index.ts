class CsePluginMock {
  constructor(private options: Record<string, unknown>) {}

  apply(compiler: unknown) {
    return compiler;
  }

  generateEntrypoints() {
    return {
      'mock-entrypoint': './mock/extension/entrypoint/',
    };
  }
}

export default CsePluginMock;
