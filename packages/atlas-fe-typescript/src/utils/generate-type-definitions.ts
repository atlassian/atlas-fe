import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import path from 'path';

/**
 * Provided dependencies (like wrm/i18n) aren't actually packages, but modules provided in runtime by the products via AMD.
 * They also don't have any TS type definitions, so TS compiler complains about not being able to process them.
 *
 * This util generates handwritten type definitions for such ocassions. You can expand the files or create new ones when needed.
 */
export const generateTypeDefinitions = async (toolbox: AtlasFeToolbox) => {
  // type definitions are located inside ../templates/types
  const files = ['wrm-i18n'];

  for (const file of files) {
    // We could just copy the files, but we can also use the template generator in the toolbox instead
    // that is always run from the root of the plugin
    await toolbox.template.generate({
      template: `${file}.d.ts`,
      target: `src/frontend/types/${file}.d.ts`,
      directory: path.resolve(__dirname, '../../', 'build', 'templates', 'types'),
    });
  }
};
