import { mocked } from 'jest-mock';
import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import { installTypeScript } from './install-typescript';

describe('install-typescript', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
      },
      plugins: { ...atlasFeToolboxMock.plugins, exists: jest.fn(() => false) },
      plugin: atlasFeToolboxMock.plugin,
      filesystem: {
        ...atlasFeToolboxMock.filesystem,
        path: jest.fn(),
      },
    };
  });

  it(`should install typescript as a dev dependency`, async () => {
    await installTypeScript(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith('typescript', { dev: true });
  });

  it(`should generate a tsconfig.json file`, async () => {
    const expectedTemplateConfig = {
      template: 'tsconfig.json.ejs',
      target: 'tsconfig.json',
      directory: expect.stringContaining('atlas-fe-typescript/build/templates'),
      props: {
        react: false,
      },
    };
    testingToolbox.plugin = {
      directory: '../..',
      hidden: false,
      commands: [],
      extensions: [],
      defaults: {},
    };
    mocked(testingToolbox.filesystem.path).mockImplementationOnce((...args) => args.join('/'));

    await installTypeScript(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(expectedTemplateConfig);
  });

  it(`should generate a tsconfig.json file with JSX support`, async () => {
    const expectedTemplateConfig = {
      template: 'tsconfig.json.ejs',
      target: 'tsconfig.json',
      directory: expect.stringContaining('atlas-fe-typescript/build/templates'),
      props: {
        react: true,
      },
    };
    testingToolbox.plugin = {
      directory: '../..',
      hidden: false,
      commands: [],
      extensions: [],
      defaults: {},
    };
    mocked(testingToolbox.filesystem.path).mockImplementationOnce((...args) => args.join('/'));
    mocked(testingToolbox.plugins.exists).mockReturnValueOnce(true);

    await installTypeScript(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(expectedTemplateConfig);
  });
});
