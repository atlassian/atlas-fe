import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import { chainWebpackConfig } from './chain-webpack-config';

describe('chain-webpack-config', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // clear webpack config
      webpackConfig.resolve.extensions.clear();
    });
  });

  it(`should configure webpack to resolve .ts files for TypeScript`, () => {
    const expectedWebpackConfig = {
      resolve: {
        extensions: ['.ts'],
      },
    };

    chainWebpackConfig(testingToolbox);

    expect(testingToolbox.webpack.resolveWebpackConfig()).toEqual(expectedWebpackConfig);
  });
});
