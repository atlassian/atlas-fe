import { AtlasFeToolbox } from '@atlassian/atlas-fe';

export const chainWebpackConfig = (toolbox: AtlasFeToolbox) => {
  // add '.ts' to the resolve list of webpack
  toolbox.webpack.chainWebpackConfig((webpackConfig) => {
    webpackConfig.resolve.extensions.add('.ts').end();
  });
};
