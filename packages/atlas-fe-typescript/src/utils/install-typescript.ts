import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import path from 'path';

export const installTypeScript = async (toolbox: AtlasFeToolbox) => {
  // install typescript
  await toolbox.packageManager.add('typescript', { dev: true });

  // generate a tsconfig.json file
  await toolbox.template.generate({
    template: 'tsconfig.json.ejs',
    target: 'tsconfig.json',
    directory: path.resolve(__dirname, '../../', 'build', 'templates'),
    props: {
      react: toolbox.plugins.exists('atlas-fe-react'),
    },
  });
};
