import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import { generateTypeDefinitions } from './generate-type-definitions';

describe('generate-type-definitions', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      plugin: atlasFeToolboxMock.plugin,
      template: {
        ...atlasFeToolboxMock.template,
        generate: jest.fn(),
      },
    };
  });

  it(`should generate the type definitions in the right folder`, async () => {
    const expectedTemplateConfig = {
      template: expect.any(String),
      target: expect.stringContaining('src/frontend/types/'),
      directory: expect.stringContaining('atlas-fe-typescript/build/templates/types'),
    };
    testingToolbox.plugin = {
      directory: '../..',
      hidden: false,
      commands: [],
      extensions: [],
      defaults: {},
    };

    await generateTypeDefinitions(testingToolbox);

    expect(testingToolbox.template.generate).toHaveBeenCalledWith(expectedTemplateConfig);
  });
});
