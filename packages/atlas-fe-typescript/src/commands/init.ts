import { AtlasFeCommand } from '@atlassian/atlas-fe';
import { configBabelPreset, configESLintPreset } from '../utils/config-preset';
import { installTypeScript } from '../utils/install-typescript';
import { generateTypeDefinitions } from '../utils/generate-type-definitions';

const COMMAND_NAME = 'init';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    await installTypeScript(toolbox);

    await configBabelPreset(toolbox);

    await configESLintPreset(toolbox);

    await generateTypeDefinitions(toolbox);

    toolbox.print.info(`${toolbox.print.checkmark} Generated TypeScript configuration`);
  },
};

export default command;
