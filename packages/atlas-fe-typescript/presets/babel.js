module.exports = () => {
  return {
    presets: [require('@atlassian/atlas-fe/presets/babel'), require('@babel/preset-typescript')],
  };
};
