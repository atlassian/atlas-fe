import { AtlasFeToolbox } from '@atlassian/atlas-fe';

export const configBabelPreset = async (toolbox: AtlasFeToolbox) => {
  await toolbox.packageJson.write((packageContent) => {
    if (!packageContent['babel'] || !Array.isArray(packageContent['babel'].presets)) {
      throw new Error('No Babel configuration found. Aborting...');
    }

    packageContent['babel'].presets.push('module:@atlassian/atlas-fe-react/presets/babel');

    return packageContent;
  });
};

export const configESLintPreset = async (toolbox: AtlasFeToolbox) => {
  await toolbox.packageJson.write((packageContent) => {
    if (!packageContent['eslintConfig'] || !Array.isArray(packageContent['eslintConfig'].extends)) {
      throw new Error('No ESLint configuration found. Aborting...');
    }

    // prettier preset always needs to be at the end of the list
    const prettierPreset = '@atlassian/eslint-config-atlas-fe/prettier';
    const reactPreset = '@atlassian/eslint-config-atlas-fe/react';
    const hasPrettierr = packageContent['eslintConfig'].extends.includes(prettierPreset);
    const filteredPresets = packageContent['eslintConfig'].extends.filter((preset: string) => preset !== prettierPreset);

    // prettier preset always needs to be at the end of the list
    packageContent['eslintConfig'].extends = [...filteredPresets, reactPreset, hasPrettierr && prettierPreset].filter(Boolean);

    return packageContent;
  });

  await toolbox.packageManager.add('@atlassian/eslint-config-atlas-fe', { dev: true });
};
