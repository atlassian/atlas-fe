import { AtlasFeToolbox } from '@atlassian/atlas-fe';

export const chainWebpackConfig = (toolbox: AtlasFeToolbox) => {
  const hasTypeScript = toolbox.plugins.exists('atlas-fe-typescript');

  // add '.jsx' and '.tsx' to the resolve list of webpack
  const extensions = ['.jsx', hasTypeScript && '.tsx'].filter(Boolean) as string[];
  extensions.forEach((extension) => {
    toolbox.webpack.chainWebpackConfig((webpackConfig) => {
      webpackConfig.resolve.extensions.add(extension).end();
    });
  });
};
