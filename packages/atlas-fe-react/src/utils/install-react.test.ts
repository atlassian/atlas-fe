import { mocked } from 'jest-mock';
import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import { installReact } from './install-react';

describe('install-typescript', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
      },
      plugins: { ...atlasFeToolboxMock.plugins, exists: jest.fn(() => false) },
      plugin: atlasFeToolboxMock.plugin,
    };
  });

  it(`should install react and react-dom as dependencies`, async () => {
    await installReact(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith(['react', 'react-dom'], {});
  });

  it(`should install  @types/react and @types/react-dom as dev dependency if TypeScript plugin is used`, async () => {
    mocked(testingToolbox.plugins.exists).mockImplementation((name) => name === 'atlas-fe-typescript');

    await installReact(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith(['@types/react', '@types/react-dom'], { dev: true });
  });
});
