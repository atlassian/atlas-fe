import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';
import { mocked } from 'jest-mock';

import { chainWebpackConfig } from './chain-webpack-config';

describe('chain-webpack-config', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock, plugins: { exists: jest.fn(() => false), run: jest.fn(), load: jest.fn() } };

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // clear webpack config
      webpackConfig.resolve.extensions.clear();
    });
  });

  it(`should configure webpack to resolve .jsx files for React`, () => {
    const expectedWebpackConfig = {
      resolve: {
        extensions: ['.jsx'],
      },
    };

    chainWebpackConfig(testingToolbox);

    expect(testingToolbox.webpack.resolveWebpackConfig()).toEqual(expectedWebpackConfig);
  });

  it(`should configure webpack to resolve .jsx and .tsx files for React`, () => {
    const expectedWebpackConfig = {
      resolve: {
        extensions: ['.jsx', '.tsx'],
      },
    };
    mocked(testingToolbox.plugins.exists).mockImplementation((name) => name === 'atlas-fe-typescript');

    chainWebpackConfig(testingToolbox);

    expect(testingToolbox.webpack.resolveWebpackConfig()).toEqual(expectedWebpackConfig);
  });
});
