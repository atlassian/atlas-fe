import { AtlasFeToolbox } from '@atlassian/atlas-fe';

export const installReact = async (toolbox: AtlasFeToolbox) => {
  const hasTypeScript = toolbox.plugins.exists('atlas-fe-typescript');

  // install react and react-dom
  await toolbox.packageManager.add(['react', 'react-dom'], {});

  // if typescript is present, install react types
  hasTypeScript && (await toolbox.packageManager.add(['@types/react', '@types/react-dom'], { dev: true }));
};
