import { AtlasFeToolbox, PackageJSON } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';
import { mocked } from 'jest-mock';

import { configESLintPreset, configBabelPreset } from './config-preset';

describe('config-preset', () => {
  let testingToolbox: AtlasFeToolbox;
  let testingPackageJSON: PackageJSON;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
      },
    };

    testingPackageJSON = {
      name: 'testing-package',
      version: '0.0.1',
      description: 'testing package',
      eslintConfig: {
        extends: ['@atlassian/eslint-config-atlas-fe/javascript', '@atlassian/eslint-config-atlas-fe/prettier'],
      },
      babel: {
        presets: ['module:@atlassian/atlas-fe/presets/babel'],
      },
    };

    mocked(testingToolbox.packageJson.write).mockImplementation(async (cb) => {
      const result = cb(testingPackageJSON);

      testingPackageJSON = result;

      return result;
    });
  });

  it(`should write the react preset on the package.json file if React plugin is used`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      babel: {
        presets: ['module:@atlassian/atlas-fe/presets/babel', 'module:@atlassian/atlas-fe-react/presets/babel'],
      },
    };

    await configBabelPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it('should throw an error if no valid Babel configuration is found', async () => {
    const wrongBabelConfig: PackageJSON = {
      ...testingPackageJSON,
      babel: 'wrong',
    };

    mocked(testingToolbox.packageJson.write).mockImplementationOnce(async (cb) => {
      const result = cb(wrongBabelConfig);
      testingPackageJSON = result;
      return result;
    });

    await expect(configBabelPreset(testingToolbox)).rejects.toThrow('No Babel configuration found. Aborting...');
  });

  it(`should write the react preset on the package.json file`, async () => {
    const expectingPackageJSON: PackageJSON = {
      ...testingPackageJSON,
      eslintConfig: {
        extends: [
          '@atlassian/eslint-config-atlas-fe/javascript',
          '@atlassian/eslint-config-atlas-fe/react',
          // prettier should go last
          '@atlassian/eslint-config-atlas-fe/prettier',
        ],
      },
    };

    await configESLintPreset(testingToolbox);

    expect(testingPackageJSON).toMatchObject(expectingPackageJSON);
  });

  it('should throw an error if no valid ESLint configuration is found', async () => {
    const wrongESLintConfig: PackageJSON = {
      ...testingPackageJSON,
      eslintConfig: 'wrong',
    };
    mocked(testingToolbox.packageJson.write).mockImplementationOnce(async (cb) => {
      const result = cb(wrongESLintConfig);
      testingPackageJSON = result;
      return result;
    });

    await expect(configESLintPreset(testingToolbox)).rejects.toThrow('No ESLint configuration found. Aborting...');
  });

  it(`should add the presets as dev dependencies to support eslint 8+`, async () => {
    await configESLintPreset(testingToolbox);

    expect(testingToolbox.packageManager.add).toHaveBeenCalledWith('@atlassian/eslint-config-atlas-fe', { dev: true });
  });
});
