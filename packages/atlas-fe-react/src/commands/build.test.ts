import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import command from './build';

describe('build command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // clear webpack config
      webpackConfig.resolve.extensions.clear();
    });
  });

  it(`should be named "build"`, () => {
    expect(command.name).toBe('build');
  });

  it(`should run the command successfully`, async () => {
    await expect(command.run(testingToolbox)).resolves.not.toThrow();
  });
});
