import { AtlasFeCommand } from '@atlassian/atlas-fe';

import { configBabelPreset, configESLintPreset } from '../utils/config-preset';
import { installReact } from '../utils/install-react';

const COMMAND_NAME = 'init';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    await installReact(toolbox);

    await configBabelPreset(toolbox);

    await configESLintPreset(toolbox);

    toolbox.print.info(`${toolbox.print.checkmark} Generated React configuration`);
  },
};

export default command;
