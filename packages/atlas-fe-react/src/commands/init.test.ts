import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import command from './init';

describe('init command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = {
      ...atlasFeToolboxMock,
      packageManager: {
        ...atlasFeToolboxMock.packageManager,
        add: jest.fn(),
      },
      plugins: {
        ...atlasFeToolboxMock.plugins,
        exists: jest.fn(),
      },
    };
  });

  it(`should be named "init"`, () => {
    expect(command.name).toBe('init');
  });

  it(`should print a message after initializing React configuration`, async () => {
    await command.run(testingToolbox);

    expect(testingToolbox.print.info).toHaveBeenCalledWith(`${testingToolbox.print.checkmark} Generated React configuration`);
  });
});
