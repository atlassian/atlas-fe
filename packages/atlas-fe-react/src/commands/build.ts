import { AtlasFeCommand } from '@atlassian/atlas-fe';
import { chainWebpackConfig } from '../utils/chain-webpack-config';

const COMMAND_NAME = 'build';

const command: AtlasFeCommand = {
  name: COMMAND_NAME,
  run: async (toolbox) => {
    chainWebpackConfig(toolbox);
  },
};

export default command;
