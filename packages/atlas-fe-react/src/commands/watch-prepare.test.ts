import { AtlasFeToolbox } from '@atlassian/atlas-fe';
import { atlasFeToolboxMock } from '@atlassian/atlas-fe/testing';

import command from './watch-prepare';

describe('watch-prepare command', () => {
  let testingToolbox: AtlasFeToolbox;

  beforeEach(() => {
    testingToolbox = { ...atlasFeToolboxMock };

    testingToolbox.webpack.chainWebpackConfig((webpackConfig) => {
      // clear webpack config
      webpackConfig.resolve.extensions.clear();
    });
  });

  it(`should be named "watch-prepare"`, () => {
    expect(command.name).toBe('watch-prepare');
  });

  it(`should run the command successfully`, async () => {
    await expect(command.run(testingToolbox)).resolves.not.toThrow();
  });
});
