import { waitFor, getDocument, queries } from 'pptr-testing-library';
import execa from 'execa';
import 'expect-puppeteer';

import { TMP_FOLDER_PATH } from '../config';

describe('Generate a project with default settings', () => {
  it('should pass all ESLint checking', async () => {
    // we need to cd into the tmp directory to run lint command in correct folder
    process.chdir(TMP_FOLDER_PATH);

    const eslintProcess = execa('yarn', ['lint']);
    eslintProcess.stdout?.pipe(process.stdout);

    const result = await eslintProcess;

    expect(result.failed).toBe(false);
  });

  it('should provide a format command to run Prettier', async () => {
    // we need to cd into the tmp directory to run format command in correct folder
    process.chdir(TMP_FOLDER_PATH);

    const prettierProcess = execa('yarn', ['format']);
    prettierProcess.stdout?.pipe(process.stdout);

    const result = await prettierProcess;

    expect(result.failed).toBe(false);
  });

  it('should load atl-general on atl.general context and render MyApp component on the page ', async () => {
    await page.goto('http://localhost:5990/refapp');

    const $document = await getDocument(page);

    await waitFor(() => queries.getByTestId($document, 'my-app'));

    expect(await queries.getByText($document, 'Testing atlas-fe CLI')).toBeTruthy();
  });
});
