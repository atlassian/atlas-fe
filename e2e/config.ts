/* eslint-disable @typescript-eslint/no-var-requires */
import path from 'path';

export const E2E_PATH = path.resolve(__dirname);

export const TMP_FOLDER_PATH = path.resolve(E2E_PATH, '__tmp__');

export enum SettingsDirectory {
  DEFAULT = 'default-settings',
  LEGACY = 'legacy-settings',
}
