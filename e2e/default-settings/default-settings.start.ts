import { SettingsDirectory } from '../config';
import { start } from '../e2e.start';

start(SettingsDirectory.DEFAULT);
