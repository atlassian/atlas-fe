import * as React from 'react';
import { createRoot } from 'react-dom/client';

import App from '../components/app';

const root = createRoot(document.querySelector('#content'));
root.render(<App />);

console.log('loading atl-general resource');
