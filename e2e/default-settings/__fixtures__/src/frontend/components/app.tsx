import * as React from 'react';
import { I18n } from '@atlassian/wrm-react-i18n';

const MyApp = () => (
  <div data-testid="my-app" className="aui-page-panel">
    <div className="aui-page-panel-inner">
      <section className="aui-page-panel-content">
        <header className="aui-page-header">
          <div className="aui-page-header-inner">
            <div className="aui-page-header-main">
              <h1>Testing atlas-fe CLI</h1>
            </div>
          </div>
        </header>

        <p>If you see this, it means the CLI is working out of the box with TypeScript and React</p>
        <p data-testid="translation">Translations: {I18n.getText('atlas.fe.refapp.hello', <strong>React</strong>, <mark>World</mark>)}</p>
      </section>
    </div>
  </div>
);

export default MyApp;
