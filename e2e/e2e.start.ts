/* eslint-disable @typescript-eslint/no-var-requires */
import util from 'util';
import fs from 'fs';
import copyfiles from 'copyfiles';
import execa from 'execa';

import { SettingsDirectory, TMP_FOLDER_PATH } from './config';

// https://github.com/Microsoft/TypeScript/issues/26048 - promisify has issues with more than one argument :)
const copyfilesAsync = util.promisify(copyfiles) as (paths: string[], options: copyfiles.Options | number) => Promise<void>;

async function createTestingEnvironment() {
  console.log('');

  if (fs.existsSync(TMP_FOLDER_PATH)) {
    console.log('E2E: DELETING OLD TESTING ENVIRONMENT');
    await util.promisify(fs.rm)(TMP_FOLDER_PATH, { recursive: true });
  }

  console.log('E2E: CREATING NEW TESTING ENVIRONMENT');
  // We're using the refapp environment for testing.
  // So here, we just copy the pom and everything inside src.
  // That's all we need to create and start a testing plugin.
  await copyfilesAsync(
    [
      './environments/refapp/pom.xml',
      './environments/refapp/src/**/*.*',

      // last path is the target path
      TMP_FOLDER_PATH,
    ],
    {
      // remove /environments/refapp folders when copy
      up: 2,
    },
  );
}

async function initializeTestingCase(settingsDirectory: SettingsDirectory) {
  console.log(`E2E: CHANGING DIRECTORY TO: ${TMP_FOLDER_PATH}`);
  process.chdir(TMP_FOLDER_PATH);

  console.log('E2E: INITIALIZING TESTING ENVIRONMENT');
  const initProcess = execa('atlas-fe', ['init', '--yes']);
  initProcess.stdout?.pipe(process.stdout);
  // wait for init process to finish before executing next command
  await initProcess;

  console.log('E2E: COPYING FIXTURE FILES');
  // we're inside /e2e/__tmp__ at this stage, so we need to pick up our fixtures from a folder UP
  await copyfilesAsync(
    [
      // path reflects that tests environment is started from file located in ${settingsDirectory}
      `../${settingsDirectory}/__fixtures__/**/*.*`,

      // last path is the target path
      '.',
    ],
    {
      // move files out of */__fixtures__ folders when copy
      up: 3,
    },
  );
}

async function startTestingEnvironment() {
  console.log('E2E: BUILDING TESTING ENVIRONMENT');
  try {
    const buildProcess = execa('yarn', ['build']);
    buildProcess.stdout?.pipe(process.stdout);
    // wait for build process to finish before executing next command
    await buildProcess;
  } catch (err) {
    console.log('E2E: ERROR WHILE BUILDING TESTING ENVIRONMENT', err);
  }

  console.log('E2E: STARTING TESTING ENVIRONMENT');
  try {
    const startProcess = execa('yarn', ['start']);
    startProcess.stdout?.pipe(process.stdout);
    await startProcess;
  } catch (err) {
    console.log('E2E: ERROR WHILE STARTING TESTING ENVIRONMENT', err);
  }
}

export async function start(settingsDirectory: SettingsDirectory) {
  await createTestingEnvironment();

  await initializeTestingCase(settingsDirectory);

  await startTestingEnvironment();
}
