/* eslint-disable @typescript-eslint/no-var-requires */
const tsPreset = require('ts-jest/jest-preset');
const puppeteerPreset = require('jest-puppeteer/jest-preset');

// Load the jest-pupeteer config
// https://github.com/smooth-code/jest-puppeteer/issues/160
process.env.JEST_PUPPETEER_CONFIG = require.resolve('./jest.puppeteer.config.js');

module.exports = {
  ...tsPreset,
  ...puppeteerPreset,
  testTimeout: 60000,
};
