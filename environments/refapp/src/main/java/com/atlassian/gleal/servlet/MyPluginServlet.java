package com.atlassian.gleal.servlet;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.osgi.framework.FrameworkUtil;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.atlassian.webresource.api.assembler.resource.ResourcePhase.DEFER;
import static java.nio.charset.StandardCharsets.UTF_8;

@Named("myPluginServlet")
public class MyPluginServlet extends HttpServlet {

  static final String RESOURCE_KEY = ":atlas-fe-base-soy";
  static final String TEMPLATE_KEY_PREFIX = "atlassian.atlas_fe.entrypoint";
  static final String ENCODING = UTF_8.name();

  @ComponentImport
  private final PageBuilderService pageBuilderService;
  @ComponentImport
  private final SoyTemplateRenderer soyTemplateRenderer;

  @Inject
  public MyPluginServlet(
    final PageBuilderService pageBuilderService,
    final SoyTemplateRenderer soyTemplateRenderer) {
    this.pageBuilderService = pageBuilderService;
    this.soyTemplateRenderer = soyTemplateRenderer;
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
    res.setContentType("text/html;charset=" + ENCODING);
    pageBuilderService.assembler().resources().requireWebResource(DEFER, getBundleName() + ":entrypoint-atl.general");
    soyTemplateRenderer.render(res.getWriter(), getBundleName() + RESOURCE_KEY, TEMPLATE_KEY_PREFIX, null);
  }

  private String getBundleName() {
    return OsgiHeaderUtil.getPluginKey(FrameworkUtil.getBundle(this.getClass()));
  }

}
