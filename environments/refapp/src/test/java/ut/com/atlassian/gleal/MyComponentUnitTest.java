package ut.com.atlassian.gleal;

import org.junit.Test;
import com.atlassian.gleal.api.MyPluginComponent;
import com.atlassian.gleal.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}