package com.atlassian.gleal.api;

public interface MyPluginComponent
{
    String getName();
}