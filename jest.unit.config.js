module.exports = {
  projects: ['<rootDir>/packages/*/jest.config.js'],
  verbose: true,
  collectCoverage: false, // set using CLI instead
  // this will make sure to collect coverage from all file, even if they don't have any test
  // https://jestjs.io/docs/en/configuration.html#collectcoveragefrom-array
  collectCoverageFrom: ['**/*.{ts,tsx}', '!**/__tests__/**', '!**/build/**', '!**/node_modules/**', '!**/testing/**'],
};
