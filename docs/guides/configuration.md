# Configuring atlas-fe CLI

The atlas-fe CLI helps you get up to speed with little effort by providing a standard folder
structure and configuration, but it's also fully configurable so that you can adapt it to your
specific needs.

[TOC]

## CLI

You can configure almost everything in atlas-fe CLI to solve your use case in a single
configuration file called `atlas-fe.config.js`.

Here's a list of all you can configure:

### Plugin key

The plugin key is needed for [WRM webpack plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin)
to work correctly. By default, atlas-fe CLI will use the artifact and group ID in your `pom.xml`
file when initializing your plugin.

```js
// atlas-fe.config.js

module.exports = {
  /**
    The plugin key, usually located in pom.xml file.
  */
  pluginKey: 'com.atlassian.acme.atlas-fe-demo',

  /* rest of config file... */
};
```

### Entry points

Entry points are JavaScript/TypeScript files that you want to process with Webpack and
transform into web-resources in order to load them via WRM.

The key of your entry point needs to be a string, and will be used as a context name for your
resources. For example, you can request the `acme.jira-issue-view` with WRM using
`WRM.require('wrc!acme.jira-issue-view')` in your templates.

```js
// atlas-fe.config.js

const path = require('path');

const FRONENT_DIR = path.join(__dirname, 'src', 'frontend');

module.exports = {
  /**
    Entry point files picked up by webpack.
  */
  entries: {
    'atl.general': path.join(FRONENT_DIR, 'entrypoints', 'atl-general.js'),
    'acme.jira-issue-view': path.join(FRONENT_DIR, 'entrypoints', 'jira-issue-view.js'),
  },

  /* rest of config file... */
};
```

### Plugins

The CLI can also be extended via plugins. In fact, some of the provided configurations are
actually plugins (TypeScript, React, CSE).

In order to intall a plugin, you need to first install its package with npm/yarn (e.g: `@atlassian/atlas-fe-typescript`), and then add it to the list of plugins in `atlas-fe.config.js`.

You can also create plugins and share them across all your projects. To learn how to do so,
read the [creating plugins for atlas-fe CLI](./creating-plugins.md) guides.

```js
// atlas-fe.config.js

module.exports = {
  /**
    atlas-fe CLI plugins to use.
  */
  plugins: [
    '@atlassian/atlas-fe-typescript',
    '@atlassian/atlas-fe-react',
    '@atlassian/atlas-fe-clientside-extensions',
    // you can also create and use your own plugins!
    '@acme/atlas-fe-shared',
  ],

  /* rest of config file... */
};
```

### I18n files

The CLI makes use of [@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader)
to transform your translation strings with webpack on development mode.

In order for `*.properties` files to be consumed, you need to specify them in your configuration:

```js
// atlas-fe.config.js
const path = require('path');

const RESOURCES_DIR = path.join(__dirname, 'src', 'main', 'resources');

module.exports = {
  /**
    Atlassian translation files (*.properties) used by i18n-properties-loader
    https://www.npmjs.com/package/@atlassian/i18n-properties-loader
  */
  i18nFiles: [path.join(RESOURCES_DIR, 'atlas-fe-demo.properties')],

  /* rest of config file... */
};
```

### Output

By default, the output directory is the `target` directory on the root of your plugin.
There might be cases when you need to modify your folder structure, including the output
directory. You can use the `output` property for that:

```js
// atlas-fe.config.js

module.exports = {
  /**
    Change the output directory used by webpack
  */
  output: path.join(__dirname, '..', 'jira-plugin', 'target', 'classes'),

  /* rest of config file... */
};
```

### Provided dependencies

Provided dependencies are web-resources that products make available to plugins to be consumed
in the form of AMD modules or Global variables. If you want to use them, you need to configure
[WRM webpack plugin provided dependencies](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin#provideddependencies-optional).

With atlas-fe CLI, you can configure them directly in `atlas-fe.config.js` as follows:

```js
// atlas-fe.config.js

module.exports = {
  /**
    An object of provided dependencies consumed by WRM webpack plugin. To learn the format required to specify the dependencies,
    read WRM webpack plugin documentation: https://bitbucket.org/atlassianlabs/atlassian-webresource-webpack-plugin/src/master/#markdown-header-provideddependencies-optional
  */
  providedDependencies: {
    'bitbucket/util/state': {
      dependency: 'com.atlassian.bitbucket.state.bitbucket-web-api:state',
      import: {
        amd: 'bitbucket/util/state',
        var: "require('bitbucket/util/state')",
      },
    },
  },

  /* rest of config file... */
};
```

### Webpack configuration

The CLI comes with a lot of things configured by default, but sometimes your use case may need
a specific webpack configuration like loaders or plugins, or even some advance output configuration.

Instead of "ejecting" like you would normally do with other CLIs, atlas-fe CLI comes with an
option to let you change the internal webpack configuration using
[webpack-chain](https://github.com/neutrinojs/webpack-chain).

e.g for adding loaders for use with Less CSS files:
```js
// atlas-fe.config.js

module.exports = {
  /**
    A function used to modify the internal webpack configuration used by atlas-fe cli.
    Use it to add your own loaders, plugins and rules, or to modify existing ones.

    The object received is a chainable webpack configuration created using webpack-chain. To learn more about its API,
    read the webpack-chain documentation: https://github.com/neutrinojs/webpack-chain
  */
  chainWebpackConfig: (webpackConfig) => {
    // prettier-ignore
    webpackConfig.module
          .rule('less')
            .test(/\.less$/)
              .use('style').loader('style-loader').end()
              .use('css').loader('css-loader').end()
              .use('less').loader('less-loader').end();
  },

  /* rest of config file... */
};
```

e.g for adding to the [WRM webpack plugin](https://www.npmjs.com/package/atlassian-webresource-webpack-plugin) configuaration:
```js
// atlas-fe.config.js

module.exports = {
  /**
    A function used to modify the internal webpack configuration used by atlas-fe cli.
    Use it to add your own loaders, plugins and rules, or to modify existing ones.

    The object received is a chainable webpack configuration created using webpack-chain. To learn more about its API,
    read the webpack-chain documentation: https://github.com/neutrinojs/webpack-chain
  */
  chainWebpackConfig: (webpackConfig) => {
      webpackConfig.plugin('WrmPlugin').tap(([config]) => {
          return [
              {
                  ...config,
                  dataProvidersMap: {
                      'example-entry-point': [
                          {
                              key: 'example-data-provider-key',
                              class: 'com.example.ExampleDataProviderClass'
                          }
                      ]
                  },
              },
          ];
      });
  }

  /* rest of config file... */
};
```

### Full example

Here's how a fully configured atlas-fe CLI would look like:

```js
// atlas-fe.config.js

const path = require('path');

const FRONENT_DIR = path.join(__dirname, 'src', 'frontend');
const RESOURCES_DIR = path.join(__dirname, 'src', 'main', 'resources');

module.exports = {
  pluginKey: 'com.atlassian.acme.atlas-fe-demo',

  entries: {
    'atl.general': path.join(FRONENT_DIR, 'entrypoints', 'atl-general.js'),
    'acme.jira-issue-view': path.join(FRONENT_DIR, 'entrypoints', 'jira-issue-view.js'),
  },

  plugins: [
    '@atlassian/atlas-fe-typescript',
    '@atlassian/atlas-fe-react',
    '@atlassian/atlas-fe-clientside-extensions',
    '@acme/atlas-fe-shared',
  ],

  i18nFiles: [path.join(RESOURCES_DIR, 'atlas-fe-demo.properties')],

  output: path.join(__dirname, '..', 'jira-plugin', 'target', 'classes'),

  providedDependencies: {
    'bitbucket/util/state': {
      dependency: 'com.atlassian.bitbucket.state.bitbucket-web-api:state',
      import: {
        amd: 'bitbucket/util/state',
        var: "require('bitbucket/util/state')",
      },
    },
  },

  chainWebpackConfig: (webpackConfig) => {
    // prettier-ignore
    webpackConfig.module
      .rule('less')
        .test(/\.less$/)
          .use('style').loader('style-loader').end()
          .use('css').loader('css-loader').end()
          .use('less').loader('less-loader').end();
  },
};
```

## Babel

The babel configuration is provided using `presets`. The core `atlas-fe` package provides the
base Babel configuration and plugins like `atlas-fe-typescript` and `atlas-fe-react` provide a
preset for each case to add support for TypeScript and React.

The presets are used in the `package.json` file as follows (the order is important):

```json
{
  "babel": {
    "presets": [
      "module:@atlassian/atlas-fe/presets/babel",
      "module:@atlassian/atlas-fe-typescript/presets/babel",
      "module:@atlassian/atlas-fe-react/presets/babel"
    ]
  }
}
```

You can use this configuration as a base and extend them however you need to. You can also move this configuration out of the `package.json` file to one of the supported [babel config files](https://babeljs.io/docs/en/config-files).

## ESLint

The eslint configuration is provided as a separate package called `@atlassian/eslint-config-atlas-fe`, and you will find it in the `package.json` file.

The rules are split in different set of rules, and they are meant to be used together in a
specific order:

```json
{
  "eslintConfig": {
    "root": true,
    "extends": [
      "@atlassian/eslint-config-atlas-fe/javascript",
      "@atlassian/eslint-config-atlas-fe/typescript",
      "@atlassian/eslint-config-atlas-fe/react",
      "@atlassian/eslint-config-atlas-fe/prettier"
    ]
  }
}
```

Make sure that Javascript is the first set of rules because it's the base for TypeScript and React,
and that Prettier is the last set of rules because it disables some of the warnings/errors depending on your prettier configuration.

You can choose only the configurations you need, and add your own set of rules too. For example:

```json
{
  "eslintConfig": {
    "root": true,
    "extends": [
      "@atlassian/eslint-config-atlas-fe/javascript",
      // no typescript
      "@atlassian/eslint-config-atlas-fe/react",
      "@atlassian/eslint-config-atlas-fe/prettier"
    ],
    "ignorePatterns": ["node_modules", "target"],
    "rules": {
      "no-console": "error",
      "no-alert": "error"
    }
  }
}
```

You can also move all this configuration out of `package.json` and to a `.eslintrc.js` file if
you prefer it.

## Prettier

A single set of prettier rules is provided by atlas-fe CLI and you can see it used in the
`package.json` file as follows:

```json
{
  "prettier": "@atlassian/atlas-fe/presets/prettier"
}
```

You can also move this configuration to any of the [configuration files](https://prettier.io/docs/en/configuration.html) supported by Prettier.

If you want to extend the default configuration, you can do so by creating a `.prettier.js` file
and export the modifications:

```js
// .prettier.js

module.exports = {
  ...require('@atlassian/atlas-fe/presets/prettier'),
  // your rules...
  semi: false,
  tabWidth: 3,
};
```
