# Creating plugins for atlas-fe CLI

[TOC]

The atlas-fe CLI helps you get up to speed with litte effort and customize it to suit your use case, and it also allows
you to create plugins for the CLI to share such configurations across different projects more easily.

In fact, a lot of the features provided by the CLI are actually plugins.

Take a look at one of the plugins in the repository for reference:

- [@atlassian/atlas-fe-typescript](https://bitbucket.org/atlassian/atlas-fe/src/master/packages/atlas-fe-typescript/).
- [@atlassian/atlas-fe-react](https://bitbucket.org/atlassian/atlas-fe/src/master/packages/atlas-fe-react/).
- [@atlassian/atlas-fe-clientside-extensions](https://bitbucket.org/atlassian/atlas-fe/src/master/packages/atlas-fe-clientside-extensions/).

## Structure of a plugin

The atlas-fe CLI uses a framework called [Gluegun](https://infinitered.github.io/gluegun/#/) that, among other features,
provides the architecture to create and consume plugins for the CLI.

A plugin consist of an `npm` package with the following folder structure:

```sh
/awesome-plugin/
├── package.json
├── commands
|  ├── build.js
|  ├── generate.js
|  └── login.js
├── templates
|  └── generate-component-template.js.ejs
└── utils
   ├── generate-utils.js
   └── login-utils.js
```

### Commands

Each file inside the `commands` folder represents a command you want to extend the CLI with. The name of the file can be
anything you want, but is recommended that it matches its execution name.

For example, a login command would be created as:

```sh
/awesome-plugin/
└── commands
   └── login.js
```

And defined as follows:

```js
// in /awesome-plugin/commads/login.js

import { login } from 'auth-library';

const command = {
  name: 'awesome-login', // the name you want to give to your command. It will then be executed as `atlas-fe awesome-login`.
  run: async (toolbox) => {
    // the functionality of your command recieves a toolbox API from Gluegun
    toolbox.print.info('Hello! logging in...');

    try {
      const { username } = await toolbox.prompt.ask({
        type: 'input',
        name: 'username',
        message: 'What is your username?',
      });

      await login(username);

      toolbox.print.info(`Logged in as ${username}`);
    } catch (err) {
      toolbox.print.error(err.message);
    }
  },
};

export default command;
```

The `name` attribute is the name of the command, and will be used as:

```sh
atlas-fe awesome-login
```

**Important consideration: the name of your command needs to be unique, or you risk of it being overriden by other plugin.
It is recommended that you namespace them with the name of your package, for example: `awesome-login` instead of `login`**.

The `run` method recieves a set of APIs by Gluegun called [toolbox](https://infinitered.github.io/gluegun/#/toolbox-api).
This toolbox provides methods to interact with the file system, generate files from templates, execute commands,
print messages and prompt input to users.

Refer to [gluegun documentation](https://infinitered.github.io/gluegun/#/plugins?id=commands) to know more about commands and toolbox.

### Utils

When the CLI runs, it first loads all the commands of all the plugins a project has installed. When using a lot of plugins, it
can become a bit slow before you see any progress.

In order to overcome this problem, it is recommended that plugins "lazy load" the content of their commands by moving them to
util files, and only import them when the command is executed.

For example, the previous `awesome-login` command can be split as follows:

```sh
/awesome-plugin/
├── commands
|  └── login.js
└── utils
   └── login-utils.js
```

```js
// in /awesome-plugin/commads/login.js

const command = {
  name: 'awesome-login',
  run: async (toolbox) => {
    const { loginUtil } = await import('../utils/login-utils'); // note the async import

    await loginUtil(toolbox);
  },
};

export default command;
```

```js
// in /awesome-plugin/utils/login-utils.js

import { login } from 'auth-library';

export const loginUtil = async (toolbox) => {
  toolbox.print.info('Hello! logging in...');

  try {
    const { username } = await toolbox.prompt.ask({
      type: 'input',
      name: 'username',
      message: 'What is your username?',
    });

    await login(username);

    toolbox.print.info(`Logged in as ${username}`);
  } catch (err) {
    toolbox.print.error(err.message);
  }
};
```

By using `await import('...')`, you make sure the content of your command will only be loaded when executing the command,
and help keep the CLI bootstrap time fast for the users.

### Templates

Gluegun provides the posibility to generate files from EJS templates using `toolbox.generate(...)`.

First, create the command, util and template files:

```sh
/awesome-plugin/
├── commands
|  └── generate.js
├── templates
|  └── generate-component-template.js.ejs
└── utils
   └── generate-utils.js
```

In this example, the name `generate` is being used for the command but it can be named in any way you want.

Next, define the template:

```js
// in /awesome-plugin/templates/generate-component-template.js.ejs

import { Component } from "awesome-framework";

export const <%= props.name %>}Component = new Component({...})
```

The template can recive properties that then can be printed on the generated file.

Finally, write your util and command:

```js
// in /awesome-plugin/utils/generate-utils.js

export const generateUtil = async (toolbox) => {
  try {
    const { name } = toolbox.parameters.options;
    const target = `app/component/${name}.js`;

    await toolbox.template.generate({
      template: 'generate-component-template.txt.ejs',
      target: target,
      props: { name },
    });

    toolbox.print.info(`${name} component created in ${target}`);
  } catch (err) {
    toolbox.print.error(err.message);
  }
};
```

```js
// in /awesome-plugin/commads/generate.js

const command = {
  name: 'awesome-generate',
  run: async (toolbox) => {
    const { generateUtil } = await import('../utils/generate-utils'); // note the async import

    await generateUtil(toolbox);
  },
};

export default command;
```

The command can then be executed as:

```sh
atlas-fe awesome-generate --name navigation
```

Refer to [gluegun documentation](https://infinitered.github.io/gluegun/#/toolbox-template) to know more about templates.

## Core commands

The CLI comes with a list of "core commands" that your plugin can extend to modify the configuration of the project
when the developer executes it:

- **init**: when initializing a project, plugins that are installed as part of the initialization can generate things on this stage.
- **build**: command executed when the developer wants to compile the project with webpack.
- **watch**: start watch mode provided by webpack-dev-server.
- **watch-prepare**: this step runs before `watch`, and its used by the CLI to compile the web-resources with
  WRM options “shouldPrepare: true”.

For example, if you wanted your plugin to modify the webpack configuration to support an `awesome-framework` when
the project is built, you would do so by creating a command with the name `build` as follows:

```sh
/awesome-plugin/
└── commands
   └── build.js
```

And define it as:

```js
// in /awesome-plugin/commads/build.js

const command = {
  name: 'build', // note that the name needs to be one of the "core commands".
  run: async (toolbox) => {
    try {
      const AwesomeFrameworkPlugin = await import('awesome-framework-webpack-plugin');

      toolbox.webpack.chainWebpackConfig((webpackConfig) => {
        webpackConfig.plugin('AwesomeFrameworkPlugin').use(AwesomeFrameworkPlugin, [
          {
            // any options for the webpack plugin go here
          },
        ]);

        toolbox.print.info(`Added awesome-framework webpack configuration`);
      });
    } catch (err) {
      toolbox.print.error(err.message);
    }
  },
};

export default command;
```

Note that the name of the command needs to match one of the "core commands" to hook to the execution.

Now, when the `build` command is executed, your command will be queue and executed in sequence with other plugins
that also provide a `build` command.

```
atlas-fe build
```

The order of execution comes defined by the order in which the plugins are defined in the [atlas-fe.config.js](./configuration.md) file.

## The atlas-fe CLI toolbox

The toolbox provided by Gluegun contains an extensive list of functions to help you interact with the file system, r
eceive input from users via prompt, generate files from templates and much more.

You can see all the functions it provides in the [gluegun toolbox documentation](https://infinitered.github.io/gluegun/#/toolbox-api).

The atlas-fe CLI extends this set of APIs to provide even more utils to interact with the internal webpack configuration,
the pom file of the project, other plugins installed and more.

Here's the definition of all the methods the CLI extends the toolbox with:

```ts
export interface AtlasFeToolbox extends GluegunToolbox {
  /** The configuration object of the project defined in `atlas-fe.config.js` */
  config: AtlasFeConfig;

  /** webpack operations */
  webpack: {
    /**
     * provides the global webpack configuration object
     * in order to modify it before passing it to webpack
     */
    chainWebpackConfig: (cb: (config: Config) => void) => void;

    /** returns the final webpack configuration */
    resolveWebpackConfig: () => WebpackConfiguration;

    /**
     * allows to add provided dependencies declarations
     * to use by WRM webpack plugin
     */
    addWrmProvidedDependencies: (dependencies: { [name: string]: unknown }) => void;
  };

  /** pom.xml file operations */
  pom: {
    /** converts an XML file to JSON **/
    parseXml: (
      xmlContent: string,
    ) => {
      content: PomAsJson;
      indent: string;
    };

    /** converts a JSON file into XML **/
    serializeXml: (content: PomAsJson, indent: string) => string;

    /** gets the content of the pom.xml file in JSON format */
    getPomContent: () => Promise<{
      content: PomAsJson;
      indent: string;
    }>;

    /** writes a PomAsJson object to the pom.xml file */
    writePomContent: (content: PomAsJson, indent: string) => Promise<void>;

    /** reads the pom.xml file and returns the full plugin key [groupdId:artifactId] */
    getPluginKeyFromPom: () => Promise<string>;
  };

  /** package.json file operations */
  packageJson: {
    /** returns the content of package.json file in JSON format */
    read: () => Promise<{ content: PackageJSON; indent: number }>;

    /**
     * writes the gien content to the package.json file of the project,
     * and returns the final result
     * */
    write: (cb: (file: PackageJSON) => PackageJSON) => Promise<PackageJSON>;
  };

  /** detects the indentation information of a given string using detect-indent npm package */
  detectIndent: (raw: string) => detectIndent.Indent;

  /** extends the Gluegun provide template API */
  template: {
    /** allows to render a template to a string without generating a file */
    render: (template: string, props?: Props) => string;
  };
}
```
