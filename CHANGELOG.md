# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2024-11-19

### Changed

- (**BREAKING**) `atlassian-webresource-webpack-plugin` upgraded to 7.0.0, no longer provides `less-transformer` as a default transformer. See https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/webresource-webpack-plugin/UPGRADE_700.md for upgrade instructions

## [0.4.0] - 2024-09-12

### Added

- Support for eslint 8 [SPFE-932](https://ecosystem.atlassian.net/browse/SPFE-932)

### Changed

- Upgraded to @atlassian/browserslist-config-server 0.3
- Upgraded to @atlassian/i18n-properties-loader 1
- Upgraded to atlassian-webresource-webpack-plugin 5
- Upgraded to eslint 8 [SPFE-932](https://ecosystem.atlassian.net/browse/SPFE-932)
- Upgraded to jest 27 [SPFE-936](https://ecosystem.atlassian.net/browse/SPFE-936)
- Upgraded to jest-puppeteer 6 [SPFE-936](https://ecosystem.atlassian.net/browse/SPFE-936)
- Upgraded to pptr-testing-library 0.8 [SPFE-936](https://ecosystem.atlassian.net/browse/SPFE-936)
- Upgraded to prettier 2.8 [SPFE-932](https://ecosystem.atlassian.net/browse/SPFE-932)
- Upgraded to puppeteer 19 [SPFE-936](https://ecosystem.atlassian.net/browse/SPFE-936)
- Upgraded to react 18
- Upgraded to react-dom 18
- Upgraded to styled-components 5
- Upgraded to terser-webpack-plugin 5
- Upgraded to ts-jest 27 [SPFE-936](https://ecosystem.atlassian.net/browse/SPFE-936)
- Upgraded to typescript 4.9 [SPFE-932](https://ecosystem.atlassian.net/browse/SPFE-932)
- Upgraded to webpack 5
- Upgraded to webpack-dev-server 4
- Upgraded all eslint plugins [SPFE-932](https://ecosystem.atlassian.net/browse/SPFE-932)
- Upgraded pipeline Docker image [SPFE-933](https://ecosystem.atlassian.net/browse/SPFE-933)
- CLI: enforcing minimum node version to be >=v20
- CLI: changed node version in generated `pom.xml` file to be v20.15.1
- CLI: upgrade frontend-maven-plugin to 1.12.1 [SPFE-933](https://ecosystem.atlassian.net/browse/SPFE-933)
- CLI: upgrade yarn to be v1.22.22 [SPFE-933](https://ecosystem.atlassian.net/browse/SPFE-933)
- Upgraded ejs to 3.1.9
- Upgraded shell-quote to 1.7.4
- Upgraded follow-redirects to 1.15.2
- Upgraded ansi-regex to 5.0.1
- Upgrade path-parse to 1.0.7
- Upgrade gson to 2.9.0
- Upgraded minimatch to 3.0.8
- Upgraded fast-xml-parser to 4.2.4 BSP-5182
- Upgraded start-server-and-test to 2.0.5
- Uses node Node.js 20

### Fixed

- Apple Silicon devices support [SPFE-933](https://ecosystem.atlassian.net/browse/SPFE-933)
- Error: error:0308010c:digital envelope routines::unsupported when using Node.js v18+ without NODE_OPTIONS=--openssl-legacy-provider

## [0.3.3]

### Fixed

- React: [SPFE-900](https://ecosystem.atlassian.net/browse/SPFE-900): Fix I18n in [products with WRM](https://bitbucket.org/atlassian/platform-module-usage/wiki/atlassian-plugins-webresource) 5.4 or higher and plugins using [Atlassian SDK (AMPS)](https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/) 8.3.0 or higher by updating [`@atlassian/wrm-react-i18n`](https://www.npmjs.com/package/@atlassian/wrm-react-i18n) to 2.0.1. Refer to [wrm-react-i18n changelog](https://bitbucket.org/atlassianlabs/fe-server/src/master/packages/wrm-react-i18n/CHANGELOG.md)

## [0.3.1]

### Fixed

- CLI: build scripts fixed for React and Typescript plugin. The CLI know should be able to configure webpack to resolve `ts`, `tsx` and `jsx` files when running `atlas-fe build`.

## [0.3.0] - 2021-05-28

### Changed

- (**BREAKING**) CSE: updated CSE to version 2.1.0. Refer to [CSE changelog](https://bitbucket.org/atlassian/atlassian-clientside-extensions/src/master/CHANGELOG.md)
  for details about the new features and breaking changes.

### Fixed

- Configuration: fixed 'FRONENT_DIR' typo in generated configuration file.

## [0.2.0] - 2021-05-03

Some breaking changes were introduced in `v0.2.0`.

Learn how to solve your issue when upgrading in the
[migrating from 0.1.0 to 0.2.0](./docs/migrations/from-0_1_0-to-0_2_0.md) guide.

### Added

- CLI: it is now possible to execute the `init` command with some options to bypass the interactive questions:
  - `--yes`: specifies that you want to use the Atlassian default settings.
- CLI: it will use the plugin key on pom.xml as name of the project when a package.json is not present instead of asking for one
- CLI: added support to Atlassian translations (i18n).
  - A new configuration property called `i18nFiles` can be used to add `*.properties` files.
    files to the internal [@atlassian/i18n-properties-loader](https://www.npmjs.com/package/@atlassian/i18n-properties-loader) configuration.
  - The `@atlassian/atlas-fe-react` plugin includes [@atlassian/wrm-react-i18n](https://www.npmjs.com/package/@atlassian/wrm-react-i18n) to support translations in React components.
  - This doesn't replace the usage of `<resource type="i18n"/>`.
- Configuration: it is now possible to configure the `output` directory used by webpack in `atlas-fe.config.js`.
- Toolbox: added `serializeXml` tool to convert a JSON into XML.
- CLI: now generates a `.nvmrc` file for plugins.

### Changed

- (**BREAKING**) Configuration: plugins now need to be specified in `atlas-fe.config.js` file, and won't be picked up automatically.
- CLI: enforcing minimum node version to be >=v12.
- CLI: changed node version in generated `pom.xml` file to be v12.20.0.

### Removed

- (**BREAKING**) Toolbox: changed name of `pom.getPomAsJson` to `pom.parseXml`.
- (**BREAKING**) CLI: the following plugins were deleted and moved to the core plugin: `eslint`, `prettier`, `babel`.

### Fixed

- Fixed installing the wrong packages when using CLI on development mode.
- Fixed ESLint glob pattern not working.
- Fixed Prettier glob pattern not working.

## [0.1.1] - 2021-01-08

### Fixed

- Removed the use of `optionalDependencies` hack for plugins depending on other packages in this monorepo when working on development.
- Fixed wrong indentation of `package.json` template file.

## [0.1.0] - 2020-12-08

This is the MVP release that provides the basic CLI configuration with a plugin system, and the most asked plugins.

### Added

- Base CLI architecture with plugin system.
- Base configuration:
  - Webpack
  - WRM webpack plugin
  - POM configuration generation
- Plugins for:
  - Babel
  - TypeScript
  - React
  - ESLint
  - Prettier
  - Client-side Extensions
