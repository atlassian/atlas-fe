module.exports = {
  verbose: true,
  testEnvironment: 'node',
  preset: 'ts-jest',
  modulePaths: ['<rootDir>/src'],
  testPathIgnorePatterns: ['/node_modules/', '<rootDir>/build/'],
  clearMocks: true,
};
